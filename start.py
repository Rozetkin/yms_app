from ymsapp import app  # , socketio
import argparse
# import eventlet
# eventlet.monkey_patch(socket=False)

parser = argparse.ArgumentParser()
parser.add_argument("--port", help="Port for debug server to listen on", default=5000)
parser.add_argument("--debug", help="Enable debug", action="store_true")
args = parser.parse_args()

if args.debug:
	from flask_debugtoolbar import DebugToolbarExtension
	import flask_profiler

	app.config["flask_profiler"] = {
	    "enabled": app.config["DEBUG"],
	    "storage": {
	        "engine": "sqlalchemy"
	    },
	    "basicAuth": {
	        "enabled": False
	    },
	}
	flask_profiler.init_app(app)
	app.config["DEBUG_TB_PROFILER_ENABLED"] = True
	app.config["DEBUG_TB_INTERCEPT_REDIRECTS"] = False
	app.config["DEBUG_TB_TEMPLATE_EDITOR_ENABLED"] = True

	toolbar = DebugToolbarExtension()
	toolbar.init_app(app)
	print(" * Flask profiling running at http://127.0.0.1:5000/flask-profiler/")
# host='82.146.43.29'
if __name__ == '__main__':
	# socketio.run(app, debug=True, host='localhost', port=args.port)
	app.run(debug=True, host='localhost', port=args.port)
