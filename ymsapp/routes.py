import os
import re
from pathvalidate import sanitize_filename
import base64
from itsdangerous.exc import BadTimeSignature, SignatureExpired, BadSignature
from flask import abort, redirect, render_template, request, send_from_directory, url_for
from flask_login import login_user, login_required, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from ymsapp import app, db, login_manager, recaptcha
from ymsapp.models import User, File
from ymsapp.decorators import admins_only, authed_only
from ymsapp.utils.tools import unique_id
from ymsapp.utils.file import allowed_file, get_url_for_file
from ymsapp.utils.security import unserialize
from ymsapp.utils import email as Email
from ymsapp.utils.group import create_db_group
from sqlalchemy.exc import DatabaseError
from ymsapp.utils.email import verify_email_address, forgot_password
from ymsapp.utils.user import get_user_groups
from ymsapp.import_google_sheet import start

@app.route('/uploads/<uid>/<filename>')
@login_required
@authed_only
def uploaded_file(uid, filename):
	file = File.query.filter_by(uid=uid, name=filename).first()
	if not file:
		return abort(404)
	file_url = get_url_for_file(file=file, user_id=current_user.id)
	if not file_url:
		return abort(403)
	if file and file.name == filename:
		path = os.path.join(app.config['UPLOAD_FOLDER'], uid)
		return send_from_directory(path, filename)
	else:
		abort(404)


@app.route('/chat')
@login_required
@authed_only
def chat():
	return render_template("chat.html")


@app.route('/admin')
@admins_only
@authed_only
def admins():
	start()
	return "Import process started"


"""@app.route('/chat/<id>', methods=['GET', 'POST'])
@login_required
@authed_only
def chat_with_user(id=None):
	if not id:
		return redirect(url_for("chat"))
	try:
		id = int(id)
	except ValueError:
		return redirect(url_for("chat"))
	if request.method == 'POST':
		try:
			file_id = request.form.get("file_id")
			if file_id:
				file_id = int(file_id)
		except ValueError:
			return redirect(url_for("chat", id=id))
		_add_message(message=request.form.get("message"),
		             to_user=id,
		             file_id=request.form.get("file_id"),
		             from_user=current_user.id)
		return redirect(url_for("chat_with_user", id=id))
	user = User.query.filter_by(id=id).first()
	if not user:
		return redirect(url_for("chat"))
	return render_template("chat_with_user.html", user=user, messages=_get_messages(from_id=current_user.id, to_id=id))"""


@app.route('/upload', methods=['GET', 'POST'])
@login_required
@authed_only
def upload_file():
	if request.method == 'POST':
		file = request.files['file']
		if file and allowed_file(file.filename):
			filename = sanitize_filename(file.filename)
			uid = unique_id()
			path = os.path.join(app.config['UPLOAD_FOLDER'], uid)
			if not os.path.exists(path):
				os.makedirs(path)
			file.save(os.path.join(path, filename))
			dbfile = File(uid=uid, name=filename, uploader_id=current_user.id, is_private=False)
			db.session.add(dbfile)
			try:
				db.session.commit()
			except:
				db.session.rollback()
			return redirect(url_for("uploaded_file", uid=uid, filename=filename))
	return render_template("upload.html")


@app.route('/login', methods=['GET', 'POST'])
def login_page():
	login = request.form.get("login")
	password = request.form.get("password")

	if request.method == 'POST':
		errors = {}
		if login and password:
			user = User.query.filter_by(login=login).first()
			if not user:
				user = User.query.filter_by(email=login).first()
			if not recaptcha.verify():
				errors['captcha'] = "Капча не пройдена"
			elif user and check_password_hash(user.password, password):
				login_user(user)
				next_page = request.args.get("next")
				if not next_page:
					next_page = "/"
				return redirect(next_page)
			else:
				errors['password'] = "Логин или пароль неверны"
		else:
			errors['password'] = "Пожалуйста введите логин и пароль"
		if len(errors) > 0:
			return render_template("login.html", errors=errors)
	return render_template("login.html", errors={})


@app.route("/reset_password", methods=["POST", "GET"])
@app.route("/reset_password/<data>", methods=["POST", "GET"])
def reset_password(data=None):
	errors = {}

	if data is not None:
		try:
			user_name = unserialize(data, max_age=1800)
		except (BadTimeSignature, SignatureExpired):
			errors["link"] = "Ваша ссылка истекла"
			return render_template("reset_password.html", errors=errors)
		except (BadSignature, TypeError, base64.binascii.Error):
			errors["token"] = "Ваш токен неверен"
			return render_template("reset_password.html", errors=errors)

		if request.method == "GET":
			return render_template("reset_password.html", errors=errors, mode="set")
		if request.method == "POST":
			password = request.form.get("password", "").strip()
			user = User.query.filter_by(login=user_name).first_or_404()

			pass_short = len(password) < 7
			if pass_short:
				errors["password"] = "Пожалуйста, выберите более длинный пароль"
				return render_template("reset_password.html", errors=errors)

			user.password = generate_password_hash(password)
			db.session.commit()
			db.session.close()
			return redirect(url_for("login_page"))

	if request.method == "POST":
		email_address = request.form["email"].strip()
		user = User.query.filter_by(email=email_address).first()

		if not user:
			return render_template(
			    "reset_password.html",
			    errors=errors,
			    info=["Если аккаунт с указаной электронной почтой существует, то на неё было отправлено письмо."])

		forgot_password(email_address, user.login)

		return render_template(
		    "reset_password.html",
		    errors=errors,
		    info=["Если аккаунт с указаной электронной почтой существует, то на неё было отправлено письмо."])
	return render_template("reset_password.html", errors=errors)


@app.route("/confirm", methods=["POST", "GET"])
@app.route("/confirm/<data>", methods=["GET"])
@login_required
def confirm(data=None):
	errors = {}
	if data and request.method == "GET":
		try:
			user_email = unserialize(data, max_age=1800)
		except (BadTimeSignature, SignatureExpired):
			errors["link"] = "Ваша ссылка истекла"
			return render_template("confirm.html", errors=errors)
		except (BadSignature, TypeError, base64.binascii.Error):
			errors["token"] = "Ваш токен неверен"
			return render_template("confirm.html", errors=errors)

		user = User.query.filter_by(email=user_email).first_or_404()
		user.verified = True
		db.session.commit()
		db.session.close()
		if current_user.is_authenticated:
			return redirect(url_for("index"))
		return redirect(url_for("login_page"))

	if not current_user.is_authenticated:
		return redirect(url_for("login_page"))

	user = User.query.filter_by(id=current_user.id).first_or_404()

	if user.verified:
		return redirect(url_for("index"))

	if data is None:
		if request.method == "POST":
			Email.verify_email_address(user.email)
			return render_template(
			    "confirm.html",
			    user=user,
			    errors=errors,
			    info=["Ваше подтверждение по электронной почте было повторно отправлено!"],
			)
		elif request.method == "GET":
			return render_template("confirm.html", errors=errors, user=user)


@app.route('/register', methods=['GET', 'POST'])
def register():
	possible_grades = ["5мл", "6мл", "7м", "8г", "8м", "9г", "9м", "10г", "10м", "11г", "11м"]

	login = request.form.get("login")
	password = request.form.get("password")
	password2 = request.form.get("password2")
	firstname = request.form.get("firstname")
	lastname = request.form.get("lastname")
	patronymic = request.form.get("patronymic")
	# as_admin = request.form.get("as_admin", False)  # will deleted soon
	grade = request.form.get("grade")
	email = request.form.get("email")
	phone_number = request.form.get("phone_number")

	errors = {}

	if request.method == 'POST':
		if not recaptcha.verify():
			errors['captcha'] = "Капча не пройдена"
		if password != password2:
			errors['password'] = "Пароли не совпадают!"
		if not verify_email_address(email):
			errors['email'] = "Электронная почта введена неверно!"
		if phone_number and not re.match(r"(\+7|7|8)+([0-9]){10}", str(phone_number)):
			errors['tel'] = "Номер телефона введен неверно!"
		else:
			if phone_number:
				phone_number = str(phone_number)
				if phone_number[0] == '8':
					phone_number = '7' + phone_number[1:]
				elif phone_number[0] == '+':
					phone_number = phone_number[1:]

		if not (login or password or password2 or firstname or lastname or grade or email):
			errors['req'] = "Пожалуйста, заполните все обязательные поля!"
		else:
			if User.query.filter_by(login=login).first():
				errors['login'] = "Данное имя пользователя уже занято!"
			elif User.query.filter_by(email=email).first():
				errors['email'] = "Данная электронная почта уже занята!"
			elif phone_number and User.query.filter_by(phone_number=phone_number).first():
				errors['tel'] = "Данный номер телефона уже занят!"
			elif User.query.filter_by(firstname=firstname, lastname=lastname, patronymic=patronymic).first():
				errors['name'] = "Данное имя уже занято!"

		if len(login) < 6:
			errors['login'] = "Имя пользователя слишком короткое! Наименьшая длина 6 символов!"
		elif len(password) < 7:
			errors['password'] = "Пароль слишком короткий! Наименьшая длина 7 символов!"

		if grade not in possible_grades:
			errors['grade'] = "Класс выбран неверно!"

		if len(errors.keys()) > 0:
			return render_template("register.html", grades=possible_grades, errors=errors)

		hash_pwd = generate_password_hash(password)
		new_user = User(login=login,
		                password=hash_pwd,
		                firstname=firstname,
		                lastname=lastname,
		                patronymic=patronymic,
		                email=email)
		if phone_number:
			new_user.phone_number = phone_number
		else:
			new_user.phone_number = None

		groups = []
		# if as_admin:
		# 	try:
		# 		groups.append(create_db_group("администратор").id)
		# 	except ValueError:
		# 		pass
		try:
			groups.append(create_db_group(grade).id)
		except ValueError:
			pass

		new_user.groups = str({"groups": groups})

		db.session.add(new_user)
		try:
			db.session.commit()
		except DatabaseError as e:
			print(e)
			db.session.rollback()
			errors.append("Some error in database...")
			return render_template("register.html", grades=possible_grades, errors=errors)
		Email.verify_email_address(new_user.email)
		login_user(new_user)
		return redirect(url_for("confirm"))
	return render_template("register.html", grades=possible_grades, errors={})


@app.route('/profile')
@app.route('/profile/<id>')
@login_required
@authed_only
def profile(id=None):
	if id is None:
		id = current_user.id
		return redirect(f"/profile/{id}")
	return render_template("profile.html", user=User.query.filter_by(id=id).first_or_404())


@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
	logout_user()
	return redirect(url_for("index"))


@app.route('/events')
@app.route('/events/<mode>')
@login_required
@authed_only
def events(mode=None):
	if mode is None:
		mode = "current"
	events_modes = ['current']
	if 14 not in get_user_groups(current_user.id):
		events_modes.append('failed')
		events_modes.append('sent')
	else:
		events_modes.append('expired')
	return render_template("events.html", mode=mode, events_modes=events_modes)


@app.route('/table')
@login_required
@authed_only
def table():
	return render_template("table.html")


@app.route('/news')
@login_required
@authed_only
def news():
	return render_template("news.html")


@app.route('/', methods=['GET', 'POST'])
def index():
	return render_template("index.html", title="YMS App")


@app.route('/favicon.ico')
def favicon():
	return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.errorhandler(404)
def not_found(e):
	return render_template("index.html")


@login_manager.unauthorized_handler
def unauthorized_callback():
	return redirect(url_for("login_page", next=request.full_path))
