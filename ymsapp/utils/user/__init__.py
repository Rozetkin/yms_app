import ast
from ymsapp import db
from ymsapp.models import User
from ymsapp.utils.file import get_file_by_id
from werkzeug.security import generate_password_hash
from flask_login import current_user
from sqlalchemy.exc import DatabaseError


def create_user(avatar_id=None,
                login=None,
                password=None,
                firstname=None,
                lastname=None,
                patronymic=None,
                email=None,
                phone_number=None,
                additional_info=None,
                groups=None):
	if not login:
		return None
	if get_user_id_by_login(login=login) is not None:
		return None
	if not password:
		return None
	if not firstname:
		return None
	if not lastname:
		return None
	if not patronymic:
		return None
	if not email:
		return None
	if get_user_id_by_email(email=str(email)) is not None:
		return None
	new_user = User()
	if avatar_id:
		if get_file_by_id(avatar_id):
			new_user.avatar = avatar_id
	new_user.login = str(login)
	new_user.password = generate_password_hash(password)
	new_user.firstname = str(firstname)
	new_user.lastname = str(lastname)
	new_user.patronymic = str(patronymic)
	new_user.email = str(email)
	if phone_number:
		try:
			new_user.phone_number = int(phone_number)
		except ValueError:
			pass
	if additional_info:
		new_user.additional_info = str(additional_info)
	if groups:
		if isinstance(groups, list):
			new_user.groups = str({"groups": groups})
	db.session.add(new_user)
	try:
		db.session.commit()
	except DatabaseError:
		db.session.rollback()
		return None
	return new_user


def get_user_id_by_email(email=None):
	if email:
		db_user = User.query.filter_by(email=email).first()
		if db_user:
			return db_user.id
		else:
			return None
	return None


def get_user_ids_by_lfp(firstname=None, lastname=None, patronymic=None):
	if firstname and lastname and patronymic:
		db_users = User.query.filter_by(firstname=firstname, lastname=lastname, patronymic=patronymic).all()
		data = []
		for db_user in db_users:
			data.append(db_user.id)
		return data
	return []


def get_user_id_by_login(login=None):
	if login:
		db_user = User.query.filter_by(login=login).first()
		if db_user:
			return db_user.id
		else:
			return None
	return None


def get_user_ids_by_group(group=None):
	if group:
		db_users = User.query.all()
		data = []
		for user in db_users:
			if group in user.groups:
				data.append(user.id)
		return data
	return None


def get_user_by_id(id=None):
	if id:
		return User.query.filter_by(id=id).first()
	else:
		return None


def get_user_groups(user_id=None):
	if user_id is None:
		user = current_user
		if user:
			data = ast.literal_eval(current_user.groups)
			return data['groups']
		else:
			return []
	else:
		user = User.query.filter_by(id=user_id).first()
		if user:
			return ast.literal_eval(user.groups)['groups']
		else:
			return []


def authed(user_id=None):
	if not user_id:
		user = current_user
	else:
		user = get_user_by_id(user_id)
	if not user:
		return False
	if user.verified:
		return True
	else:
		return False


def is_teacher(user_id=None):
	groups = get_user_groups(user_id=user_id)
	if not bool(groups):
		return False
	elif 14 in groups:
		return True
	else:
		return False


def is_admin(user_id=None):
	groups = get_user_groups(user_id=user_id)
	if not bool(groups):
		return False
	elif 13 in groups:
		return True
	else:
		return False
