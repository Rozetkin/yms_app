from ymsapp import db
from ymsapp.models import Group
from sqlalchemy.exc import DatabaseError


def create_db_group(group_name=None):
	if group_name is not None:
		group_name = group_name.lower()
		db_group = Group.query.filter_by(name=group_name).first()
		if not db_group:
			db_group = Group(name=group_name)
			db.session.add(db_group)
			try:
				db.session.commit()
			except DatabaseError:
				db.session.rollback()
				return None
		return db_group
	return None
