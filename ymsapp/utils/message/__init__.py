from ymsapp.models import User, Group_chat, Message
from ymsapp.utils.group_chat import get_group_chat_members
from sqlalchemy import text, desc
from ymsapp import db
from datetime import datetime


def _get_messages(from_id=None, to_id=None, to_group=False):
	if from_id and to_id:
		try:
			from_id = int(from_id)
			to_id = int(to_id)
		except ValueError:
			return None
		if to_group:
			to_group_chat_id = to_id
			all_messages = Message.query.filter_by(to_group_chat_id=to_group_chat_id).order_by("time").all()
			db_group_chat = Group_chat.query.filter_by(id=to_group_chat_id).first()
			if not db_group_chat:
				return None
			if from_id in get_group_chat_members(db_group_chat.id):
				return all_messages
			else:
				return None
		db_user = User.query.filter_by(id=to_id).first()
		if not db_user:
			return None
		messages_1 = Message.query.filter_by(from_id=from_id, to_id=to_id)
		if from_id != to_id:
			messages_2 = Message.query.filter_by(from_id=to_id, to_id=from_id)
			all_messages = messages_1.union(messages_2).order_by(desc(text('6'))).all()
		else:
			all_messages = messages_1.order_by("time").all()
		return all_messages


def _add_message(message_text=None, file_id=None, from_user=None, to_user=None):
	if (message_text or file_id) and from_user and to_user:
		db_message = Message(from_id=from_user, to_id=to_user, time=datetime.now())
		if message_text:
			db_message.text = message_text
		if file_id:
			db_message.file_id = file_id
		db.session.add(db_message)
		try:
			db.session.commit()
		except:
			db.session.rollback()
