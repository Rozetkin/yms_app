import uuid
from ymsapp import db
from datetime import datetime
import pytz
from ymsapp.utils.response import reply
from sqlalchemy.exc import DatabaseError


localtz =  pytz.timezone("Europe/Moscow")

def get_time():
	utc_now = pytz.utc.localize(datetime.utcnow())
	msk_now = utc_now.astimezone(pytz.timezone("Europe/Moscow"))
	return msk_now


def unique_id():
	return hex(uuid.uuid4().time)[2:-1]


def b64_unique_id():
	return uuid.uuid4().hex


def get_item(data, item_id, result, whitelist=[], aval_vals=[], typeof=None, length=2048):
	item = data.get(item_id)

	if item not in aval_vals:
		errors = []

		if "emptiness" not in whitelist:
			empty = False
			if isinstance(item, str):
				if item is None:
					empty = True
			elif isinstance(item, list):
				if not item:
					empty = True

			if empty:
				errors.append("{} must not be empty")

		if "validity" not in whitelist:
			valid = True
			if not isinstance(item, list):
				if not item:
					valid = False

			if not valid:
				errors.append("{} is not valid")

		if "length" not in whitelist:
			size = True
			if len(str(item)) > length:
				size = False

			if not size:
				errors.append("{} is too long")

		if typeof is not None:
			if not isinstance(item, typeof):
				errors.append("{} is of wrong type")

		if errors:
			result["ok"] = False
			errors = [item.format(item_id) for item in errors]
			for item in errors:
				result["errors"].append(item)
			return reply(errors=errors)

	return item


def db_push(item=None, id_label=None, success_msg="Push succeeded"):
	if item:
		db.session.add(item)
	try:
		db.session.commit()
	except DatabaseError as e:
		print(e)
		db.session.rollback()
		return reply(errors=["Some error in db..."], resp_code=500)
	else:
		return reply(success=True, data=[success_msg], id_label=id_label, item_id=item.id, resp_code=200)
