#!/usr/bin/env python3

# from message import *
from itertools import permutations


def normalize(input):
	return input.replace('-', ',').replace('&', ',')


def get_split_labels(input):
	# Searching for the labels in the string
	labels = []

	for input_piece in input.split(','):
		label = {"upper-case": []}

		# Upper-case
		for i, item in enumerate(input_piece):
			if item.isupper():
				label["upper-case"].append(i)
		label["upper-case"].append(len(input_piece))
		labels.append(label)

	return labels


def split(input, labels):
	# Splitting input via labels
	splitted_input = []

	# Upper-case
	for i, input_piece in enumerate(input.split(',')):
		splitted_input.append([])
		prev_item = labels[i]["upper-case"][0]
		for item in labels[i]["upper-case"][1:]:
			splitted_input[-1].append(input_piece[prev_item:item])
			prev_item = item

	separated_input = []
	for i, item in enumerate(splitted_input):
		if len(item) == 6:
			separated_input.append(item[:3])
			separated_input.append(item[3:])
		elif len(item) == 4:
			separated_input.append(item[:2])
			separated_input.append(item[2:])
		else:
			separated_input.append(item)

	to_del = []
	for i, item in enumerate(separated_input):
		if len(item) == 0:
			to_del.append(i)

	to_del.reverse()
	for item in to_del:
		del (separated_input[item])

	return separated_input


def clean(input):
	# Cleaning input
	cleared_input = []

	for item in input:
		cleared_input.append([])
		for element in item:
			cleared_element = element
			cleared_element = cleared_element.replace(' ', '')
			cleared_element = cleared_element.replace('.', '')
			cleared_input[-1].append(cleared_element)

	return cleared_input


def clamp(input):
	# Clampting input
	clamped_input = []

	clamped_input.append(input[0])
	for i, item in enumerate(input[1:]):
		clamped_input.append(item[:1])

	return clamped_input


def is_equal(str1, str2):
	diff_cnt = abs(len(str2) - len(str1))
	for i in range(max(len(str1), len(str2))):
		if min(len(str1), len(str2)) - 1 < i:
			break
		if str2[i] != str1[i]:
			diff_cnt += 1
	return True if diff_cnt <= 1 else False


def match(input, db):
	# Matching input with db

	result_matches = []
	for i, input_user in enumerate(input):
		good_matches = []
		close_matches = []
		for var in list(permutations(input_user)):
			matches = []

			lastname_matches = []
			if len(var) > 0:
				# Finding the last-name matches
				for user in db:
					if is_equal(user[0], var[0]):
						lastname_matches.append(user)
				matches = lastname_matches

			firstname_matches = []
			if len(var) > 1:
				# Finding the first-name matches
				for user in lastname_matches:
					if user[1][:1] == var[1][:1]:
						firstname_matches.append(user)
				matches = firstname_matches

			patronymic_matches = []
			if len(var) > 2:
				# Finding the patronymic matches
				for user in firstname_matches:
					if user[2][:1] == var[2][:1]:
						patronymic_matches.append(user)
				matches = patronymic_matches

			if len(matches) > 0:
				for item in matches:
					if item not in good_matches:
						good_matches.append(item)
			if len(firstname_matches) > 0:
				for item in firstname_matches:
					if item not in close_matches:
						close_matches.append(item)

		if len(good_matches) == 1:
			result_matches.append({"user": good_matches[0]})
		elif len(good_matches) == 0:
			result_matches.append({
			    "user": None,
			    "error": [f"There is no user like '{input[i]}' that in the database.", close_matches]
			})
		else:
			result_matches.append({
			    "user": None,
			    "error": [f"There are some possible variants for '{input[i]}' in the database.", good_matches]
			})
	return result_matches


def _parse(input, db, exceptions, match_bool=True):
	for key, item in exceptions.items():
		input = input.replace(key, item)

	if input == '':
		return [{"user": None}]

	# Parsing input
	parsing_result = None

	normalized_input = normalize(input)
	split_labels = get_split_labels(normalized_input)
	splitted_input = split(normalized_input, split_labels)
	cleaned_input = clean(splitted_input)
	clamped_input = cleaned_input
	if match_bool:
		parsing_result = match(clamped_input, db)
	else:
		parsing_result = clamped_input

	return parsing_result


def parse(input=None, db=None, exceptions=None):
	return _parse(input, db, exceptions, match_bool=True)
