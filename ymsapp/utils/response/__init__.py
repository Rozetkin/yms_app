import ast
from flask import jsonify, make_response


def reply(success=False,
          errors=None,
          data=None,
          event_id=None,
          message_id=None,
          group_chat_id=None,
          user_id=None,
          file_id=None,
          id_label=None,
          item_id=None,
          resp_code=400):
	js_data = ast.literal_eval('{ }')
	js_data["success"] = success
	if errors:
		js_data["errors"] = errors
	if data:
		js_data["data"] = data
	if event_id:
		js_data["event_id"] = event_id
	if message_id:
		js_data["message_id"] = message_id
	if group_chat_id:
		js_data["group_chat_id"] = group_chat_id
	if user_id:
		js_data["user_id"] = user_id
	if file_id:
		js_data["file_id"] = file_id
	if item_id:
		js_data[id_label] = item_id
	try:
		return jsonify(js_data), resp_code
	except BaseException as e:
		return make_response(str(e)), 500
