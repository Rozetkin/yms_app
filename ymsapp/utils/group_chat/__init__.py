import ast
from ymsapp.models import Group_chat


def get_group_chat_members(group_chat_id):
	db_group = Group_chat.query.filter_by(id=int(group_chat_id)).first()
	if db_group:
		json = ast.literal_eval(db_group.members)
		members = json['members']
	else:
		members = []
	return members
