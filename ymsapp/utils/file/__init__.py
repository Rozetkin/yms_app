import ast
import os
from ymsapp import app
from ymsapp.models import File
from flask import url_for
from flask_login import current_user

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
	return '.' in filename and \
        filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def get_file_by_id(file_id=None):
	if file_id:
		file = File.query.filter_by(id=file_id).first()
		return file
	return None


def get_file_members(file=None):
	if file:
		file_members = ast.literal_eval(file.members)['members']
		if file.uploader_id not in file_members:
			file_members.append(file.uploader_id)
		return file_members
	else:
		return []


def get_url_for_file(file=None, user_id=None):
	if not user_id and current_user.is_authenticated:
		user_id = current_user.id
	if file and user_id:
		if not file.is_private or user_id in get_file_members(file=file):
			return url_for('uploaded_file', uid=file.uid, filename=file.name)
		else:
			return None
	else:
		return None


def delete_file(uid=None, filename=None):
	if not uid or not filename:
		return None
	dir_path = os.path.join(app.config['UPLOAD_FOLDER'], uid)
	file_path = os.path.join(dir_path, filename)
	if not os.path.exists(file_path):
		return None
	os.remove(file_path)
	if os.path.exists(dir_path) and os.walk(dir_path)[3] is [] and os.walk(dir_path)[2] is []:
		os.rmdir(dir_path)
	return True
