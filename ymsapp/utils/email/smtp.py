import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from socket import timeout


def get_smtp(host, port, username=None, password=None, TLS=None, SSL=None, auth=None):
	if SSL is None:
		smtp = smtplib.SMTP(host, port, timeout=3)
	else:
		smtp = smtplib.SMTP_SSL(host, port, timeout=3)

	if TLS:
		smtp.starttls()

	if auth:
		smtp.login(username, password)
	return smtp


def sendmail(addr, html, text, subject):
	mailfrom_addr = "info@yumshdist.ru"
	mailfrom_addr = "{} <{}>".format("Inform system", mailfrom_addr)

	data = {
	    "host": "82.146.43.29",
	    "port": int(25),
	}
	username = "info@yumshdist.ru"
	password = "super_secret_password_for_imap1060123"
	TLS = False
	SSL = False
	auth = True

	if username:
		data["username"] = username
	if password:
		data["password"] = password
	if TLS:
		data["TLS"] = TLS
	if SSL:
		data["SSL"] = SSL
	if auth:
		data["auth"] = auth

	try:
		smtp = get_smtp(**data)
		if html is not None:
			msg = MIMEMultipart('alternative')
			part1 = MIMEText(text, 'plain')
			part2 = MIMEText(html, 'html')
			msg.attach(part1)
			msg.attach(part2)
		else:
			msg = MIMEText(text)

		msg["Subject"] = subject
		msg["From"] = mailfrom_addr
		msg["To"] = addr

		smtp.sendmail(msg["From"], [msg["To"]], msg.as_string())
		smtp.quit()
		return True, "Email sent"
	except smtplib.SMTPException as e:
		return False, str(e)
	except timeout:
		return False, "SMTP server connection timed out"
	except Exception as e:
		return False, str(e)
