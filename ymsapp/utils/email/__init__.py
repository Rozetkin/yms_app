from flask import url_for
from ymsapp.utils.email import smtp
from ymsapp.utils.security import serialize
import re

EMAIL_REGEX = r"(^[^@\s]+@[^@\s]+\.[^@\s]+$)"


def sendmail(addr, html, text, subject):
	return smtp.sendmail(addr, html, text, subject)


def forgot_password(email, user_name):
	token = serialize(user_name)
	url = url_for("reset_password", _external=True)
	forgot_html = open("ymsapp/templates/emails/reset.html")
	html = forgot_html.read().replace("{url}", url).replace("{token}", token)  # need to write reset_password route
	text = f"Восстановить пароль просто! Просто нажмите на ссылку ниже и следуйте указаниям!\n {url}/{token}"
	forgot_html.close()

	return sendmail(email, html, text, "Сброс пароля Yumshdist.ru")


def verify_email_address(addr):
	token = serialize(addr)
	url = url_for("confirm", _external=True)
	verify_html = open("ymsapp/templates/emails/register.html")
	html = verify_html.read().replace("{url}", url_for("confirm", _external=True)).replace("{token}", token)
	verify_html.close()
	text = f"Здравствуйте. Вы создали аккаунт на сайте yumshdist.ru. Чтобы подтвердить ваш аккаунт, нажмите на кнопку ниже.\n {url}/{token}"

	return sendmail(addr, html, text, "Подтверждние аккаунта Yumshdist.ru")


def user_created_notification(addr, name, password):
	text = """Для вас был создан аккаунт для yumshdist.ru по адресу {url}. \n\nЛогин: {name}\nПароль: {password}""".format(
	    url=url_for("index", _external=True),
	    name=name,
	    password=password,
	)
	return sendmail(addr, text)


def check_email_format(email):
	return bool(re.match(EMAIL_REGEX, email))
