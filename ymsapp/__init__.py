import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_recaptcha import ReCaptcha
from flask_wtf.csrf import CSRFProtect
import pathlib
# from flask_socketio import SocketIO
import humanize

_t = humanize.i18n.activate("ru_RU")

app = Flask(__name__)
app.secret_key = 'yms very secret key'
UPLOAD_FOLDER = str(pathlib.Path().absolute()) + "/uploads"
if not os.path.exists(UPLOAD_FOLDER):
	os.makedirs(UPLOAD_FOLDER)
app.config["DEBUG"] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqldb://yms_server:1060123@umsctf.cf/ymshappdb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['SQLALCHEMY_POOL_RECYCLE'] = 280
app.config['CAPTCHA_ENABLE'] = True
app.config['RECAPTCHA_SITE_KEY'] = "6Le3SOYUAAAAACy9ZSUaflIMpU4-6_zh6rympZZw"
app.config['RECAPTCHA_SECRET_KEY'] = "6Le3SOYUAAAAAK7aCbHQkHBIETZyS9kRXcyLiiJF"
app.config['RECAPTCHA_THEME'] = "light"
app.config['SESSION_TYPE'] = 'sqlalchemy'
db = SQLAlchemy(app)
csrf = CSRFProtect(app)
recaptcha = ReCaptcha()
# socketio = SocketIO(app)
recaptcha.init_app(app)
login_manager = LoginManager(app)
login_manager.init_app(app)
login_manager.login_view = 'login'

from ymsapp import routes, models # noqa F401
from ymsapp.api import messages, users, group_chats, files, events, tasks, subjects, groups  # noqa F401
from ymsapp.import_google_sheet import start  # noqa F401

db.create_all()
# start()
