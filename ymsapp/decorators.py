import functools
from flask import abort, redirect, request, url_for
from flask_login import current_user
from flask_socketio import disconnect
from ymsapp.utils.user import authed, is_admin, is_teacher


def multi_threading(func):
	@functools.wraps(func)
	def wrapper(*args_, **kwargs_):
		import threading
		func_thread = threading.Thread(target=func, args=tuple(args_), kwargs=kwargs_)
		func_thread.start()
		return func_thread

	return wrapper


def authed_only(f):
	"""
    Decorator that requires the user to be authenticated
    :param f:
    :return:
    """
	@functools.wraps(f)
	def authed_only_wrapper(*args, **kwargs):
		if authed():
			return f(*args, **kwargs)
		else:
			if (request.content_type == "application/json" or request.accept_mimetypes.best == "text/event-stream"):
				abort(403)
			else:
				return redirect(url_for("confirm", next=request.full_path))

	return authed_only_wrapper


def teachers_only(f):
	"""
    Decorator that requires the user to be authenticated
    :param f:
    :return:
    """
	@functools.wraps(f)
	def teachers_only_wrapper(*args, **kwargs):
		if is_teacher():
			return f(*args, **kwargs)
		else:
			if (request.content_type == "application/json" or request.accept_mimetypes.best == "text/event-stream"):
				abort(403)
			else:
				return redirect(url_for("login_page", next=request.full_path))

	return teachers_only_wrapper


def admins_only(f):
	"""
    Decorator that requires the user to be authenticated and an admin
    :param f:
    :return:
    """
	@functools.wraps(f)
	def admins_only_wrapper(*args, **kwargs):
		if is_admin():
			return f(*args, **kwargs)
		else:
			if request.content_type == "application/json":
				abort(403)
			else:
				return redirect(url_for("login_page", next=request.full_path))

	return admins_only_wrapper


def authed_io_only(f):
	@functools.wraps(f)
	def wrapped(*args, **kwargs):
		if not current_user.is_authenticated:
			disconnect()
		else:
			return f(*args, **kwargs)

	return wrapped
