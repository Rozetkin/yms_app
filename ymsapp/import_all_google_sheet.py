from ymsapp import db, app
from ymsapp.utils.user import (create_user, get_user_id_by_login, get_user_by_id, get_user_ids_by_lfp, is_teacher)
from ymsapp.models import Task, Subject, Event
import requests
import colorama
import ymsapp.links
from random import randrange
from transliterate import translit
from datetime import datetime
import os
import time
import ast
from ymsapp.decorators import multi_threading

colorama.init()


@multi_threading
def delete_update_file():
	time.sleep(10)
	os.remove('updating_running')


def start():
	if os.path.exists("updating_running"):
		return True
	runfile = open("updating_running", "w")
	runfile.close()
	app.config['UPDATE_SHEET'] = False
	name_links_s = ymsapp.links.name_links_s
	subject_links_s = ymsapp.links.subject_links_s
	type_links_s = ymsapp.links.type_links_s
	name_links, subject_links, type_links = [], [], []

	def unify(string_item=None):
		if string_item:
			return string_item.replace('-', ' ').replace('.', ' ').replace(' ', '').lower()
		return None

	for type_link_s in type_links_s:
		type, link = unify(type_link_s[0]), type_link_s[1]
		is_skip = False
		for type_link in type_links:
			if type in type_link:
				is_skip = True
				break
		if is_skip:
			continue
		type_links.append([type, link])

	for name_link_s in name_links_s:
		name, link = unify(name_link_s[0]), name_link_s[1]
		is_skip = False
		for name_link in name_links:
			if name in name_link:
				is_skip = True
				break
		if is_skip:
			continue
		name_links.append([name, link])

	for subject_link_s in subject_links_s:
		subject, link = unify(subject_link_s[0]), subject_link_s[1]
		is_skip = False
		for subject_link in subject_links:
			if subject in subject_link:
				is_skip = True
				break
		if is_skip:
			continue
		subject_links.append([subject, link])

	api_key_file = open("api.key", "r")
	api_key = api_key_file.read()
	api_key_file.close()

	tmpdata = {'types': [], 'names': [], 'subjects': []}

	def create_db_subject(name=None):
		if name:
			db_subject = Subject.query.filter_by(name=name).first()
			if db_subject:
				return db_subject
			subject = Subject(name=name)
			db.session.add(subject)
			db.session.commit()
			return subject
		else:
			return None

	def create_db_event(google_sheet_line=None,
	                    subject_id=None,
	                    types=None,
	                    description=None,
	                    links=None,
	                    author=None,
	                    time=None,
	                    additional_author=None,
	                    task_id=None,
	                    for_groups=None,
	                    unparsed_data={
	                        'types': [],
	                        'names': [],
	                        'subjects': []
	                    }):
		if not types or not for_groups:
			if not types and unparsed_data['types'] is not [] and not for_groups:
				pass
			else:
				return None
		if google_sheet_line:
			test_event = Event.query.filter_by(google_sheet_line=google_sheet_line).first()
			if test_event:
				if test_event.subject != subject_id:
					test_event.subject = subject_id
				if test_event.types != str({"types": types}):
					test_event.types = str({"types": types})
				if test_event.description != description:
					test_event.description = description
				if test_event.links != links:
					test_event.links = links
				if test_event.author != author:
					test_event.author = author
				if test_event.time is not time:
					test_event.time = time
				if test_event.additional_author != additional_author:
					test_event.additional_author = additional_author
				if test_event.task != task_id:
					test_event.task = task_id
				if test_event.for_groups != str({"for_groups": for_groups}):
					test_event.for_groups = str({"for_groups": for_groups})
				test_event.unparsed_data = str(unparsed_data)
				db.session.commit()
				return test_event
		new_event = Event(google_sheet_line=google_sheet_line,
		                  subject=subject_id,
		                  types=str({"types": types}),
		                  description=description,
		                  links=links,
		                  author=author,
		                  time=time,
		                  additional_author=additional_author,
		                  task=task_id,
		                  for_groups=str({"for_groups": for_groups}),
		                  unparsed_data=str(unparsed_data))
		db.session.add(new_event)
		db.session.commit()
		return new_event

	def create_db_task(google_sheet_line=None, text=None, method=None):
		if text and method:
			if google_sheet_line:
				test_event = Event.query.filter_by(google_sheet_line=google_sheet_line).first()
				if test_event:
					if test_event.task:
						task = Task.query.filter_by(id=test_event.task).first()
						if task.text != text:
							task.text = text
						if task.method != method:
							task.method = method
						db.session.commit()
						return task
			task = Task(text=text, method=method)
			db.session.add(task)
			db.session.commit()
			return task
		else:
			return None

	def gen_teacher_login():
		while True:
			login = "teacher_" + str(randrange(0, 500, 1))
			db_user_id = get_user_id_by_login(login=login)
			if not db_user_id:
				return login

	def make_user(teacher=None):
		if teacher is None or teacher == " ":
			return None
		patronymic = translit(teacher[0], reversed=True)
		firstname = translit(teacher[1], reversed=True)
		lastname = translit(teacher[2], reversed=True)
		email = patronymic + "." + firstname + "." + lastname + "@ymsapp.com"
		ids = get_user_ids_by_lfp(firstname=teacher[1], lastname=teacher[2], patronymic=teacher[0])
		teacher_id = None
		for id in ids:
			if is_teacher(user_id=id):
				teacher_id = id
				break
		if teacher_id is not None:
			db_user = get_user_by_id(id=teacher_id)
		else:
			db_user = create_user(avatar_id=None,
			                      login=gen_teacher_login(),
			                      password="super_secret_teachers_password",
			                      firstname=teacher[1],
			                      lastname=teacher[2],
			                      patronymic=teacher[0],
			                      email=email,
			                      groups=["Teachers"])
		if db_user is not None:
			return db_user.id
		else:
			print("Error in create user!!!")
			raise ValueError

	def find_name_link(name=None):
		if name:
			for i in range(len(name_links)):
				find_link = [unify(name_links[i][0]), unify(name_links[i][1])]
				if unify(name) in find_link:
					if name_links[i][1] == "ignore":
						tmpdata['names'].append(name)
						return " "
					return name_links[i][1]
			return None
		return None

	def add_name_link(name=None, link=None):
		if name and link:
			if link == "st":
				link = name
			elif link == "ignore":
				name_links.append([unify(name), link])
				print(f"'{name}' was ignored!")
				return
			name_links.append([unify(name), link])
			print(f"Link '{link}' for '{name}' added!")

	def find_type_link(type=None):
		if type:
			for i in range(len(type_links)):
				find_link = [unify(type_links[i][0]), unify(type_links[i][1])]
				if unify(type) in find_link:
					if type_links[i][1] == "ignore":
						tmpdata['types'].append(type)
						return " "
					return type_links[i][1]
			return None
		return None

	def add_type_link(type=None, link=None):
		if type and link:
			if link == "st":
				link = name
			elif link == "ignore":
				type_links.append([unify(type), link])
				print(f"'{type}' was ignored!")
				return
			type_links.append([unify(type), link])
			print(f"Link '{link}' for '{type}' added!'")

	def find_subject_link(subject=None):
		if subject:
			for i in range(len(subject_links)):
				find_link = [unify(subject_links[i][0]), unify(subject_links[i][1])]
				if unify(subject) in find_link:
					if subject_links[i][1] == "ignore":
						tmpdata['subjects'].append(subject)
						return " "
					return subject_links[i][1]
			return None
		return None

	def add_subject_link(subject=None, link=None):
		if subject and link:
			if link == "st":
				link = name
			elif link == "ignore":
				subject_links.append([unify(subject), link])
				print(f"'{subject}' was ignored!")
				return
			subject_links.append([unify(subject), link])
			print(f"Link '{link}' for '{subject}' added!")

	params = (('majorDimension', 'COLUMNS'), ('key', api_key))
	response = requests.get(
	    'https://sheets.googleapis.com/v4/spreadsheets/1vjEIBFRK8jLxxDVk30Valklf0TcGs3-T3Y2rj8LIgt0/values/полная+база',
	    params=params)
	js_sheet = ast.literal_eval(response.text)
	r = []

	for i in range(len(js_sheet["values"][0])):
		temp = []
		for j in range(len(js_sheet["values"])):
			try:
				text = js_sheet["values"][j][i]
				if text is None:
					text = ""
			except IndexError:
				text = ""
			temp.append(text)
		r.append(temp)
	for i in range(len(r)):
		if i == 0:
			print("\033[39m\n\n****************************************************************")
			continue
		print(f"\r\033[K\033[31m -- DB updating process. Please wait! Current line is: {i}/{len(r)-1}", end=' ')

		tmpdata = {'types': [], 'names': [], 'subjects': []}
		for_groups = [str(r[i][0])]
		try:
			time = datetime.strptime(str(r[i][1])[2:].strip(" ") + ".2020 " + str(r[i][2]), "%d.%m.%Y %H:%M")
		except ValueError:
			try:
				time = datetime.strptime(str(r[i][1])[2:].strip(" ") + ".2020 " + str(r[i][2]), "%d.%m.%Y %H.%M")
			except ValueError:
				time = datetime.now()

		subject = str(r[i][3]).replace('-', ' ').split(', ')[0].lower()
		if not find_subject_link(subject=subject) and subject != '':
			print("\n\n\n**********************************************************")
			print(f"Link for subject '{subject}' not found! (line in google sheet: {i+1})")
			add_subject_link(subject=subject, link=str(input("Enter link: ")))
			print("**********************************************************\n\n\n")
		subject = find_subject_link(subject=subject)
		o_types = str(r[i][4]).lower().split(", ")
		types = []
		for type in o_types:
			if not find_type_link(type=type) and type != '':
				print("\n\n\n**********************************************************")
				print(f"Link for type '{type}' not found! (line in google sheet: {i+1})")
				add_type_link(type=type, link=str(input("Enter link: ")))
				print("**********************************************************\n\n\n")
			type = find_type_link(type=type)
			if type != " " and type is not None and type not in types:
				types.append(type)
		description = str(r[i][5])
		links = str(r[i][6])
		task_text = str(r[i][7])
		task_method = str(r[i][8])
		o_teachers = str(r[i][9]).replace("&", ", ").replace("-", ", ").split(", ")
		teachers = []
		for teacher in o_teachers:
			if not find_name_link(name=teacher) and teacher != '':
				print("\n\n\n**********************************************************")
				print(f"Link for name '{teacher}' not found! (line in google sheet: {i+1})")
				add_name_link(name=teacher, link=str(input("Enter link: ")))
				print("**********************************************************\n\n\n")
			teacher = find_name_link(name=teacher)
			if teacher != "" and teacher != " " and teacher is not None:
				teachers.append(teacher.split(" "))
		t_ids = []
		if teachers == []:
			if tmpdata['names'] is []:
				continue
		else:
			for teacher in teachers:
				t_ids.append(make_user(teacher=teacher))
		task = None
		if "дедлайн" in types or task_text:
			task = create_db_task(google_sheet_line=i + 1, text=task_text, method=task_method)
			if "дедлайн" not in types and task:
				types.append("дедлайн")
		subject = create_db_subject(subject)
		if len(t_ids) == 0:
			t_ids.append(None)
		if len(t_ids) == 1:
			t_ids.append(None)
		if task is None:
			task_id = None
		else:
			task_id = task.id
		create_db_event(google_sheet_line=i + 1,
		                subject_id=subject.id,
		                types=types,
		                description=description,
		                links=links,
		                time=time,
		                author=t_ids[0],
		                additional_author=t_ids[1],
		                task_id=task_id,
		                for_groups=for_groups,
		                unparsed_data=tmpdata)
	print("\033[39m\r\033[K ++ successful")
	print("****************************************************************\n")
	file_t = open("links.txt", "a")
	write_str = "name_links = " + str(name_links) + "\n" + "subject_links = " + str(
	    subject_links) + "\n" + "type_links = " + str(type_links) + "\n"
	file_t.write(write_str)
	file_t.close()
	delete_update_file()
	return True
	# raise ValueError
