from flask import request
from flask_login import current_user
from ymsapp import app, db, csrf
from datetime import datetime
from ymsapp.models import User, File, Message, Group_chat
from ymsapp.utils.message import _get_messages
from ymsapp.utils.file import get_file_by_id, get_url_for_file
from ymsapp.utils.response import reply
from ymsapp.utils.group_chat import get_group_chat_members
from sqlalchemy.exc import DatabaseError
import locale


@app.route('/api/v1/messages/add', methods=['POST'])
@csrf.exempt
def add_message():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	from_id = current_user.id
	to_group_chat_id = data.get("to_group_chat_id")
	to_id = data.get("to_id")
	text = data.get("text")
	file_id = data.get("file_id")
	if from_id is None:
		return reply(errors=["You are not logged in"], resp_code=401)
	if not text and not file_id:
		return reply(errors=["Text or file_id must not be empty"])
	if not to_id and not to_group_chat_id:
		return reply(errors=["To_id or to_group_chat_id must not be empty"], resp_code=400)
	if text and len(text) > 2048:
		return reply(errors=["Text too long"])
	if file_id:
		db_file = File.query.filter_by(id=file_id).first()
		if not db_file.id:
			return reply(errors=["File_id is not valid"])
	db_message = Message(from_id=from_id, text=text, file_id=file_id, time=datetime.now())
	if to_id:
		db_to_id = User.query.filter_by(id=to_id).first()
		if not db_to_id:
			return reply(errors=["To_id is not valid"])
		db_message.to_id = to_id
	else:
		db_group_chat = Group_chat.query.filter_by(id=to_group_chat_id).first()
		if not db_group_chat:
			return reply(errors=["To_group_chat_id is not valid"])
		if from_id not in get_group_chat_members(group_chat_id=to_group_chat_id):
			return reply(errors=["You are not a member of this group_chat"], resp_code=403)
		db_message.to_group_chat_id = to_group_chat_id
	db.session.add(db_message)
	try:
		db.session.commit()
	except DatabaseError:
		db.session.rollback()
		return reply(errors=["Some error in db..."], resp_code=500)
	else:
		return reply(success=True, data=["Message successfully added"], message_id=db_message.id, resp_code=200)


@app.route('/api/v1/messages/delete', methods=['POST'])
@csrf.exempt
def del_message():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	from_id = current_user.id
	message_id = data.get("message_id")
	if not message_id:
		return reply(errors=["Message_id must not be empty"])
	db_message = Message.query.filter_by(id=message_id, from_id=from_id).first()
	if not db_message:
		return reply(errors=["Message_id is not valid"])
	if not db_message.id or db_message.is_deleted:
		return reply(errors=["Message_id is not valid"])
	db_message.is_deleted = True
	try:
		db.session.commit()
	except DatabaseError:
		db.session.rollback()
		return reply(errors=["Some error in db..."], resp_code=500)
	else:
		return reply(success=True, data=["Message successfully deleted"], message_id=db_message.id, resp_code=200)


@app.route('/api/v1/messages/edit', methods=['POST'])
@csrf.exempt
def edit_message():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	from_id = current_user.id
	message_id = data.get("message_id")
	text = data.get("text")
	file_id = data.get("file_id")
	if from_id is None:
		return reply(errors=["You are not logged in"], resp_code=401)
	if not text and not file_id:
		return reply(errors=["Text or file_id must not be empty"])
	if not message_id:
		return reply(errors=["Message_id must not be empty"])
	if text:
		if len(text) > 2048:
			return reply(errors=["Text too long"])
	if file_id:
		db_file = File.query.filter_by(id=file_id).first()
		if not db_file.id:
			return reply(errors=["File_id is not valid"])
	db_message = Message.query.filter_by(id=message_id, from_id=from_id).first()
	if not db_message:
		return reply(errors=["Message_id is not valid"])
	if not db_message.id or db_message.is_deleted:
		return reply(errors=["Message_id is not valid"])
	if file_id:
		db_message.file_id = file_id
	if text:
		db_message.text = text
	db_message.is_changed = True
	try:
		db.session.commit()
	except DatabaseError:
		db.session.rollback()
		return reply(errors=["Some error in db..."], resp_code=500)
	else:
		return reply(success=True, data=["Message successfully edited"], message_id=db_message.id, resp_code=200)


@app.route('/api/v1/messages/get', methods=['POST'])
@csrf.exempt
def get_message():
	locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	user_id = current_user.id
	message_id = data.get("message_id")
	if message_id is None:
		return reply(errors=["Message_id must not be empty"])
	message = Message.query.filter_by(id=message_id).first()
	if not message:
		return reply(errors=["Message_id is not valid"])
	if message.from_id != current_user.id and message.to_id != user_id:
		if not message.to_group_chat_id:
			return reply(errors=["Message_id is not valid"])
		if user_id not in get_group_chat_members(message.to_group_chat_id):
			return reply(errors=["Message_id is not valid"])
	json_resp = []
	if message.is_deleted:
		text = None
		file_url = None
	else:
		text = message.text
		file_url = get_url_for_file(get_file_by_id(file_id=message.file_id))
	js_data = {}
	js_data["id"] = message.id
	js_data["text"] = text
	js_data["file_url"] = file_url
	js_data["from_id"] = message.from_id
	js_data["to_id"] = message.to_id
	js_data["to_group_chat_id"] = message.to_group_chat_id
	js_data["time"] = message.time.strftime("%d %b %y  %H:%M")
	js_data["is_changed"] = message.is_changed
	js_data["is_deleted"] = message.is_deleted
	json_resp.append(js_data)
	return reply(success=True, data=json_resp, message_id=message.id, resp_code=200)


@app.route('/api/v1/messages/get_all', methods=['POST'])
@csrf.exempt
def get_messages():
	locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	from_id = current_user.id
	to_id = data.get("to_id")
	to_group_chat_id = data.get("to_group_chat_id")
	group_message = False
	if from_id is None:
		return reply(errors=["You are not logged in"], resp_code=401)
	if to_group_chat_id is not None:
		to_id = to_group_chat_id
		group_message = True
	if to_id is None:
		return reply(errors=["to_id or to_group_chat_id must not be empty"], resp_code=400)
	messages = _get_messages(from_id=from_id, to_id=to_id, to_group=group_message)
	if messages is None:
		if group_message:
			return reply(errors=["To_group_chat_id is not valid"], resp_code=400)
		return reply(errors=["To_id is not valid"], resp_code=400)
	json_resp = []
	for message in messages:
		if message.is_deleted:
			text = None
			file_url = None
		else:
			text = message.text
			file_url = get_url_for_file(get_file_by_id(file_id=message.file_id))
		js_data = {}
		js_data["id"] = message.id
		js_data["text"] = text
		js_data["file_url"] = file_url
		js_data["from_id"] = message.from_id
		js_data["to_id"] = message.to_id
		js_data["to_group_chat_id"] = message.to_group_chat_id
		js_data["time"] = message.time.strftime("%d %b %y  %H:%M")
		js_data["is_changed"] = message.is_changed
		js_data["is_deleted"] = message.is_deleted
		json_resp.append(js_data)
	return reply(success=True, data=json_resp, resp_code=200)
