from flask import request
from flask_login import current_user
from ymsapp import app
from ymsapp.models import File, Task
from ymsapp.utils.response import reply
from ymsapp.utils.tools import get_item, db_push
import ast


@app.route('/api/v1/tasks/add', methods=['POST'])
def add_task():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	text = get_item(data, "text", result, length=4096)
	method = get_item(data, "method", result, length=1024)
	file_ids = get_item(data, "file_ids", result, whitelist=["emptiness"], typeof=list)
	if not result["ok"]:
		return reply(errors=result["errors"])

	for file_id in file_ids:
		db_file = File.query.filter_by(id=file_id).first()
		if not db_file:
			return reply(errors=[f"File with id '{file_id}' does not exists"])
	js_files = {"file_ids": file_ids}

	task = Task(text=text, method=method, file_ids=js_files)

	commit = db_push(item=task, id_label="task_id", success_msg="Task successfully added")
	return commit


@app.route('/api/v1/tasks/delete', methods=['POST'])
def del_task():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	task_id = get_item(data, "task_id", result)
	if not result["ok"]:
		return reply(errors=result["errors"])

	db_task = Task.query.filter_by(id=task_id).first()

	if not db_task or not db_task.id:  # or not db_task.is_deleted:
		return reply(errors=["Task_id is not valid"])

	# db_task.is_deleted = True

	commit = db_push(id_label="task_id", success_msg="Task successfully deleted")
	return commit


@app.route('/api/v1/tasks/edit', methods=['POST'])
def edit_task():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	task_id = get_item(data, "task_id", result)
	text = get_item(data, "text", length=4096)
	method = get_item(data, "method", length=1024)
	file_ids = get_item(data, "file_ids", result, whitelist=["emptiness"], typeof=list)
	if not result["ok"]:
		return reply(errors=result["errors"])

	db_task = Task.query.filter_by(id=task_id).first()
	if not db_task or not db_task.id:  # or db_task.is_deleted:
		return reply(errors=["Task_id is not valid"])

	for file_id in file_ids:
		db_file = File.query.filter_by(id=file_id).first()
		if not db_file:
			return reply(errors=[f"File with id '{file_id}' does not exists"])
	js_files = {"file_ids": file_ids}

	task = Task(text=text, method=method, file_ids=js_files)

	commit = db_push(item=task, success_msg="Task successfully edited")
	return commit


@app.route('/api/v1/tasks/get', methods=['POST'])
def get_task():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	task_id = get_item(data, "task_id", result)
	if not result["ok"]:
		return reply(errors=result["errors"])

	task = Task.query.filter_by(id=task_id).first()
	if not task:
		return reply(errors=["Task_id is not valid"])

	# if task.is_deleted:

	js_data = {}
	js_data["id"] = task.id
	js_data['text'] = task.text
	js_data["method"] = task.method
	js_data["file_ids"] = ast.literal_eval(task.file_ids)

	return reply(success=True, data=js_data, id_label="task_id", resp_code=200)


@app.route('/api/v1/tasks/get_all', methods=['POST'])
def get_tasks():
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	json_resp = []
	tasks = Task.query.filter_by()
	for task in tasks:
		# if task.is_deleted:
		js_data = {}
		js_data["id"] = task.id
		js_data["method"] = task.method
		js_data['text'] = task.text
		js_data["file_ids"] = ast.literal_eval(task.file_ids)
		json_resp.append(js_data)
	return reply(success=True, data=json_resp, resp_code=200)
