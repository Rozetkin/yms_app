import ast
import os
from pathvalidate import sanitize_filename
from flask import render_template, request
from flask_login import current_user
from ymsapp import app, db
from ymsapp.models import User, File
from ymsapp.utils.file import get_file_by_id, get_url_for_file, allowed_file, delete_file
from ymsapp.utils.tools import unique_id
from ymsapp.utils.response import reply
from sqlalchemy.exc import DatabaseError
import shutil


@app.route('/test_api_file')
def test_files_api():
	return render_template("file.html")


@app.route('/api/v1/files/upload', methods=['POST'])
def api_upload_file():
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	if 'file' not in request.files:
		return reply(errors=["File must not be empty"])
	file = request.files.get("file")
	is_private = request.form.get("is_private")
	if file and allowed_file(file.filename):
		filename = sanitize_filename(file.filename)
		if filename == "":
			return reply(errors=["Filename is empty"])
		uid = unique_id()
		path = os.path.join(app.config['UPLOAD_FOLDER'], uid)
		if not os.path.exists(path):
			os.makedirs(path)
		file.save(os.path.join(path, filename))
		db_file = File(uid=uid, name=filename, uploader_id=current_user.id, is_private=is_private)
		db.session.add(db_file)
		try:
			db.session.commit()
		except DatabaseError:
			db.session.rollback()
			shutil.rmtree(path)
			return reply(errors=["Some error in db..."], resp_code=500)
		return reply(success=True, data=["File added successfully"], file_id=db_file.id, resp_code=201)
	if file:
		return reply(errors=["Uploading this file is not allowed"], resp_code=403)
	else:
		return reply(errors=["Something strange error"], resp_code=500)


@app.route('/api/v1/files/delete', methods=['POST'])
def api_delete_file():
	data = request.get_json()
	file_id = data.get("file_id")
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	if not file_id:
		return reply(errors=["File_id must not be empty"])
	db_file = File.query.filter_by(id=file_id).first()
	if not db_file:
		return reply(errors=["File_id in not valid"])
	if db_file.uploader_id != current_user.id:
		return reply(errors=["You do not have enough permissions to edit this file"], resp_code=403)
	file_uid, file_name = db_file.uid, db_file.name
	db.session.remove(db_file)
	try:
		db.session.commit()
	except DatabaseError:
		db.session.rollback()
		return reply(errors=["Some error in db..."], resp_code=500)
	if not delete_file(file_uid, file_name):
		print("Error on deleting file {file_name} at {uid}!!!")
	return reply(success=True, data=["File has been successfully deleted"], resp_code=200)


@app.route('/api/v1/files/get', methods=['POST'])
def get_file():
	data = request.get_json()
	file_id = data.get("file_id")
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	if not file_id:
		return reply(errors=["File_id must not be empty"])
	file = get_file_by_id(file_id=file_id)
	if not file:
		return reply(errors=["File_id is not valid"])
	file_url = get_url_for_file(file=file, user_id=current_user.id)
	if not file_url:
		return reply(errors=["You do not have enough permissions to get this file"], resp_code=403)
	json_resp = []
	js_data = {}
	js_data["id"] = file.id
	js_data["name"] = file.name
	js_data["url"] = file_url
	js_data["uploader_id"] = file.uploader_id
	if file.uploader_id == current_user.id:
		js_data["is_private"] = file.is_private
		js_data["members"] = ast.literal_eval(file.members)["members"]
	json_resp.append(js_data)
	return reply(success=True, data=json_resp, resp_code=200)


@app.route('/api/v1/files/get_all', methods=['POST'])
def get_files():
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	db_files = File.query.all()
	json_resp = []
	for file in db_files:
		file_url = get_url_for_file(file=file, user_id=current_user.id)
		if file_url:
			js_data = {}
			js_data["id"] = file.id
			js_data["name"] = file.name
			js_data["url"] = file_url
			js_data["uploader_id"] = file.uploader_id
			if file.uploader_id == current_user.id:
				js_data["is_private"] = file.is_private
				js_data["members"] = ast.literal_eval(file.members)["members"]
			json_resp.append(js_data)
	return reply(success=True, data=json_resp, resp_code=200)


@app.route('/api/v1/files/edit', methods=['POST'])
def edit_file():
	data = request.get_json()
	file_id = data.get("file_id")
	is_private = data.get("is_private")
	members = data.get("members")
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	file = get_file_by_id(file_id=file_id)
	if not file:
		return reply(errors=["File does not exists"])
	if file.uploader_id != current_user.id:
		return reply(errors=["You do not have enough permissions to edit this file"], resp_code=403)
	if not members and not is_private:
		return reply(errors=["Members or is_private must not be null"])
	if members:
		for member in members:
			db_member = User.query.filter_by(id=member)
			if not db_member:
				return reply(errors=["User with id '{member}' does not exists"])
		js_members = {"members": members}
		file.members = js_members
	if is_private is not None:
		file.is_private = is_private
	try:
		db.session.commit()
	except DatabaseError:
		db.session.rollback()
		return reply(errors=["Some error in db..."], resp_code=500)
	return reply(success=True, data=["File have been changed successfully"], file_id=file_id, resp_code=200)
