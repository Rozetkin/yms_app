from flask import request
from flask_login import current_user
from ymsapp import app
from ymsapp.models import Subject
from ymsapp.utils.response import reply
from ymsapp.utils.tools import get_item, db_push


@app.route('/api/v1/subjects/add', methods=['POST'])
def add_subject():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	name = get_item(data, "name", result, length=128)
	if not result["ok"]:
		return reply(errors=result["errors"])

	subject = Subject(name=name)

	commit = db_push(item=subject, id_label="subject_id", success_msg="Subject successfully added")
	return commit


@app.route('/api/v1/subjects/delete', methods=['POST'])
def del_subject():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	subject_id = get_item(data, "subject_id", result)
	if not result["ok"]:
		return reply(errors=result["errors"])

	db_subject = Subject.query.filter_by(id=subject_id).first()

	if not db_subject or not db_subject.id:  # or not db_subject.is_deleted:
		return reply(errors=["Subject_id is not valid"])

	# db_subject.is_deleted = True

	commit = db_push(id_label="subject_id", success_msg="Subject successfully deleted")
	return commit


@app.route('/api/v1/subjects/edit', methods=['POST'])
def edit_subject():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	subject_id = get_item(data, "subject_id", result)
	name = get_item(data, "name", result, length=128)
	if not result["ok"]:
		return reply(errors=result["errors"])

	db_subject = Subject.query.filter_by(id=subject_id).first()
	if not db_subject or not db_subject.id:  # or db_subject.is_deleted:
		return reply(errors=["Subject_id is not valid"])

	subject = Subject(name=name)

	commit = db_push(item=subject, success_msg="Subject successfully edited")
	return commit


@app.route('/api/v1/subjects/get', methods=['POST'])
def get_subject():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	subject_id = get_item(data, "subject_id", result)
	if not result["ok"]:
		return reply(errors=result["errors"])

	subject = Subject.query.filter_by(id=subject_id).first()
	if not subject:
		return reply(errors=["Subject_id is not valid"])

	# if subject.is_deleted:

	js_data = {}
	js_data["id"] = subject.id
	js_data["name"] = subject.name

	return reply(success=True, data=js_data, id_label="subject_id", resp_code=200)


@app.route('/api/v1/subjects/get_all', methods=['POST'])
def get_subjects():
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	json_resp = []
	subjects = Subject.query.filter_by()
	for subject in subjects:
		# if subject.is_deleted:
		js_data = {}
		js_data["id"] = subject.id
		js_data["name"] = subject.name
		json_resp.append(js_data)
	return reply(success=True, data=json_resp, resp_code=200)
