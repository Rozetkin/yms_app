from flask import request
from flask_login import current_user
from ymsapp import app, db
from ymsapp.models import File, Task, Subject, Event, User_event_tag
from ymsapp.utils.response import reply
from ymsapp.utils.tools import get_item, db_push, get_time, localtz
from ymsapp.utils.user import get_user_groups
from sqlalchemy.exc import DatabaseError
#import pytz
import ast
import datetime


@app.route('/api/v1/events/add', methods=['POST'])
def add_event():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	file_ids = get_item(data, "file_ids", result, whitelist=["emptiness"], typeof=list)
	subject_id = get_item(data, "subject_id", result)
	types = get_item(data, "types", result, typeof=list)
	description = get_item(data, "description", result)
	links = get_item(data, "links", result)
	text = get_item(data, "text", result)
	for_groups = get_item(data, "for_groups", result)
	unparsed_data = get_item(data, "unparsed_data", result, length=2048)
	author = current_user.id
	if not result["ok"]:
		return reply(errors=result["errors"])

	db_file = Subject.query.filter_by(id=subject_id).first()
	if not db_file:
		return reply(errors=["Subject_id is not valid"])

	task = None
	if "дедлайн" in types:
		for file_id in file_ids:
			db_file = File.query.filter_by(id=file_id).first()
			if not db_file:
				return reply(errors=["{file_id} does not exist"])
		js_files = {"file_ids": file_ids}
		task = Task(text=text, file_ids=js_files)
		db_push(item=task, id_label="task_id", success_msg="Task successfully added")

	event = Event(subject=subject_id,
	              types=str({"types": types}),
	              description=description,
	              links=links,
	              author=author,
	              for_groups=for_groups,
	              unparsed_data=unparsed_data,
	              task=task.id if task else None)

	commit = db_push(item=event, id_label="event_id", success_msg="Event successfully added")
	return commit


@app.route('/api/v1/events/delete', methods=['POST'])
def del_event():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	event_id = get_item(data, "event_id", result)
	if not result["ok"]:
		return reply(errors=result["errors"])

	db_event = Event.query.filter_by(id=event_id).first()

	if not db_event or not db_event.id:  # or not db_event.is_deleted:
		return reply(errors=["Event_id is not valid"])

	# db_event.is_deleted = True

	commit = db_push(id_label="event_id", success_msg="Event successfully deleted")
	return commit


@app.route('/api/v1/events/edit', methods=['POST'])
def edit_event():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	event_id = get_item(data, "event_id", result)
	file_ids = get_item(data, "file_ids", result, whitelist=["emptiness"], typeof=list)
	subject_id = get_item(data, "subject_id", result)
	types = get_item(data, "types", result, typeof=list)
	description = get_item(data, "description", result)
	links = get_item(data, "links", result)
	text = get_item(data, "text", result)
	for_groups = get_item(data, "for_groups", result)
	unparsed_data = get_item(data, "unparsed_data", result, length=2048)
	if not result["ok"]:
		return reply(errors=result["errors"])

	db_file = Subject.query.filter_by(id=subject_id).first()
	if not db_file:
		return reply(errors=["Subject_id is not valid"])

	db_event = Event.query.filter_by(id=event_id).first()
	if not db_event or not db_event.id:  # or db_event.is_deleted:
		return reply(errors=["Event_id is not valid"])

	task = None
	if "дедлайн" in types:
		if not db_event.task:
			for file_id in file_ids:
				db_file = File.query.filter_by(id=file_id).first()
				if not db_file:
					return reply(errors=["File with id '{file_id}' does not exists"])
			js_files = {"file_ids": file_ids}
			task = Task(text=text, file_ids=js_files, unparsed_data={"text": text})
			db_push(item=task, success_msg="Task successfully added")
		else:
			db_task = Task.query.filter_by(id=db_event.task).first()
			if not db_task:  # or db_task.is_deleted:
				return reply(errors=["Task_id is not valid"])
			if text:
				db_task.text = text
			for file_id in file_ids:
				db_file = File.query.filter_by(id=file_id).first()
				if not db_file:
					return reply(errors=["File with id '{file_id}' does not exists"])
			db_task.file_ids = {"file_ids": file_ids}
			db_push(success_msg="Commit successful")

	db_event.task = task.id if task else None
	db_event.subject = subject_id
	db_event.types = str({"types": types})
	db_event.description = description
	db_event.links = links
	db_event.for_groups = str({"for_groups": for_groups})
	db_event.unparsed_data = str({
	    "subject": unparsed_data["subject"],
	    "types": unparsed_data["types"],
	    "time": unparsed_data["time"],
	    "description": unparsed_data["description"],
	    "links": unparsed_data["links"],
	    "authors": unparsed_data["authors"],
	    "for_groups": unparsed_data["for_groups"]
	})

	commit = db_push(id_label="event_id", success_msg="Event successfully edited")
	return commit


@app.route('/api/v1/events/get', methods=['POST'])
def get_event():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	event_id = get_item(data, "event_id", result)
	is_admin_get = get_item(data, "is_admin_get", result, aval_vals=[None])
	if not result["ok"]:
		return reply(errors=result["errors"])

	event = Event.query.filter_by(id=event_id).first()
	if not event:
		return reply(errors=["Event_id is not valid"])

	user_groups = get_user_groups(current_user.id)
	if not (((13 in user_groups) or (14 in user_groups)) and is_admin_get):
		for group in user_groups:
			if group in ast.literal_eval(event.for_groups)["for_groups"]:
				break
		else:
			return reply(errors=["Permission denied"], resp_code=403)

	# if event.is_deleted:
	db_tags = User_event_tag.query.filter_by(user_id=current_user.id, event_id=event_id).first()
	if db_tags:
		tags = ast.literal_eval(db_tags)['tags']
	else:
		tags = []

	js_data = {}
	js_data["id"] = event.id
	js_data["google_sheet_line"] = event.google_sheet_line
	js_data["subject"] = event.subject
	js_data["types"] = ast.literal_eval(event.types)["types"]
	js_data["time"] = event.time
	js_data["description"] = event.description
	js_data["links"] = event.links
	js_data["author"] = event.author
	js_data["additional_author"] = event.additional_author
	js_data["task"] = event.task
	js_data["for_groups"] = ast.literal_eval(event.for_groups)["for_groups"]
	js_data["unparsed_data"] = ast.literal_eval(event.unparsed_data)
	js_data["tags"] = tags

	return reply(success=True, data=js_data, id_label="event_id", resp_code=200)


@app.route('/api/v1/events/get_all', methods=['POST'])
def get_events():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	is_admin_get = get_item(data, "is_admin_get", result, aval_vals=[None])
	if not result["ok"]:
		return reply(errors=result["errors"])

	user_groups = get_user_groups(current_user.id)

	json_resp = []
	events = Event.query.all()
	all_db_tags = User_event_tag.query.filter_by(user_id=current_user.id).all()
	for event in events:
		tags = []

		# If event for this user, than we give it to him
		for group in user_groups:
			if group in ast.literal_eval(event.for_groups)['for_groups']:
				break
		else:
			# If this user is teacher or admin, than we give him access too
			if not (((13 in user_groups) or (14 in user_groups)) and is_admin_get):
				continue

		tags = []
		# Checking database for existing tags for this event
		for db_tags in all_db_tags:
			if db_tags.event_id == event.id:
				tags = ast.literal_eval(db_tags.tags)['tags']
				break

		if localtz.localize(event.time) < get_time():
			if "expired" not in tags:
				tags.append("expired")
			if "current" in tags:
				tags.remove("current")
		else:
			if "current" not in tags:
				tags.append("current")
			if "expired" in tags:
				tags.remove("expired")

		# if event.is_deleted:
		js_data = {}
		js_data["id"] = event.id
		js_data["google_sheet_line"] = event.google_sheet_line
		js_data["subject"] = event.subject
		js_data["types"] = ast.literal_eval(event.types)["types"]
		js_data["time"] = event.time
		js_data["description"] = event.description
		js_data["links"] = event.links
		js_data["author"] = event.author
		js_data["additional_author"] = event.additional_author
		js_data["task"] = event.task
		js_data["for_groups"] = ast.literal_eval(event.for_groups)["for_groups"]
		js_data["unparsed_data"] = ast.literal_eval(event.unparsed_data)
		js_data["tags"] = tags
		json_resp.append(js_data)
	return reply(success=True, data=json_resp, resp_code=200)


@app.route('/api/v1/events/add_tag', methods=['POST'])
def add_event_tag():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	event_id = get_item(data, "event_id", result)
	tag = get_item(data, "tag", result)

	event = Event.query.filter_by(id=event_id).first()
	if not event:
		return reply(errors=["Event_id is not valid"])
	db_tag = User_event_tag.query.filter_by(user_id=current_user.id, event_id=event.id).first()
	if db_tag is None:
		db_tag = User_event_tag(user_id=current_user.id, event_id=event.id, tags=str({"tags": [tag]}))
		db.session.add(db_tag)
	else:
		db_tags = ast.literal_eval(db_tag.tags)['tags']
		if tag not in db_tags:
			db_tags.append(tag)
		db_tag.tags = str({"tags": db_tags})
	try:
		db.session.commit()
	except DatabaseError:
		db.session.rollback()
		return reply(errors=["Some error in db..."], resp_code=500)
	return reply(success=True, data=["Tag added successfully"], resp_code=200)


@app.route('/api/v1/events/del_tag', methods=['POST'])
def del_event_tag():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	event_id = get_item(data, "event_id", result)
	tag = get_item(data, "tag", result)

	event = Event.query.filter_by(id=event_id).first()
	if not event:
		return reply(errors=["Event_id is not valid"])
	db_tag = User_event_tag.query.filter_by(user_id=current_user.id, event_id=event.id).first()
	if db_tag is None:
		return reply(success=True, data=["Tag deleted successfully"], resp_code=200)
	else:
		db_tags = ast.literal_eval(db_tag.tags)['tags']
		if tag in db_tag:
			db_tags.remove(tag)
		db_tag.tags = str({"tags": db_tags})
	try:
		db.session.commit()
	except DatabaseError:
		db.session.rollback()
		return reply(errors=["Some error in db..."], resp_code=500)
	return reply(success=True, data=["Tag deleted successfully"], resp_code=200)
