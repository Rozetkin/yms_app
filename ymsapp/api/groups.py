from flask import request
from flask_login import current_user
from ymsapp import app
from ymsapp.models import Group
from ymsapp.utils.response import reply
from ymsapp.utils.tools import get_item


@app.route('/api/v1/groups/get', methods=['POST'])
def get_group():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	group_id = get_item(data, "group_id", result)
	if not result["ok"]:
		return reply(errors=result["errors"])

	group = Group.query.filter_by(id=group_id).first()
	if not group:
		return reply(errors=["Group_id is not valid"])

	js_data = {}
	js_data["id"] = group.id
	js_data["name"] = group.name

	return reply(success=True, data=js_data, id_label="group_id", resp_code=200)


@app.route('/api/v1/groups/get_all', methods=['POST'])
def get_groups():
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	json_resp = []
	groups = Group.query.filter_by()
	for group in groups:
		js_data = {}
		js_data["id"] = group.id
		js_data["name"] = group.name
		json_resp.append(js_data)
	return reply(success=True, data=json_resp, resp_code=200)
