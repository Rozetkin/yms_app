import ast
from flask import request
from flask_login import current_user
from ymsapp import app, db
from ymsapp.models import User, File, Group_chat
from ymsapp.utils.file import get_file_by_id, get_url_for_file
from ymsapp.utils.response import reply
from sqlalchemy.exc import DatabaseError


@app.route('/api/v1/group_chats/add', methods=['POST'])
def add_group_chat():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	members = data.get("members")
	if not isinstance(members, list):
		return reply(errors=["Members list is not valid"])
	if len(members) < 2:
		return reply(errors=["This is not a group chat"])
	admins = data.get("admins")
	if not isinstance(admins, list):
		return reply(errors=["Admins list is not valid"])
	if len(admins) < 1:
		return reply(errors=["Group must have at least 1 admin"])
	name = data.get("name")
	avatar_id = data.get("avatar_id")
	if not name:
		return reply(errors=["Name of the group chat must not be null"])
	if len(name) > 128:
		return reply(errors=["Name of the group chat too long"])
	db_group_chat = Group_chat(name=name)
	if avatar_id:
		db_avatar = File.query.filter_by(id=avatar_id).first()
		if db_avatar:
			db_group_chat.avatar = int(avatar_id)
		else:
			return reply(errors=["Avatar_id is not valid"])
	js_members = {"members": members}
	js_admins = {"admins": admins}
	db_group_chat.members = js_members
	db_group_chat.admins = js_admins
	db.session.add(db_group_chat)
	try:
		db.session.commit()
	except DatabaseError:
		db.session.rollback()
		return reply(errors=["Some error in db..."], resp_code=500)
	return reply(success=True, data=["Group chat has been added successfully"], group_id=db_group_chat.id, resp_code=200)


@app.route('/api/v1/group_chats/delete', methods=['POST'])
def del_group_chat():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	group_chat_id = data.get("group_chat_id")
	db_group_chat = Group_chat.query.filter_by(id=group_chat_id).first()
	if not db_group_chat:
		return reply(errors=["Group_chat_id is invalid"])
	js_admins = ast.literal_eval(db_group_chat.admins)
	admins = js_admins["admins"]
	user_id = current_user.id
	if user_id not in admins:
		return reply(errors=["You are not admin in this group"], resp_code=403)
	db.session.delete(db_group_chat)
	try:
		db.session.commit()
	except DatabaseError:
		db.session.rollback()
		return reply(errors=["Some error in db..."], resp_code=500)
	return reply(success=True, data=["Group chat has been deleted successfully"], group_id=group_chat_id, resp_code=200)


@app.route('/api/v1/group_chats/edit', methods=['POST'])
def edit_group_chat():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	from_id = current_user.id
	group_chat_id = data.get("group_chat_id")
	name = data.get("name")
	add_members = data.get("add_members")
	# del_members = data.get("del_members")
	add_admins = data.get("add_admins")
	# del_admins = data.get("del_admins")
	avatar_id = data.get("avatar_id")
	if not group_chat_id:
		return reply(errors=["Group_chat_id must not be empty"])
	db_group_chat = Group_chat.query.filter_by(id=group_chat_id).first()
	if not db_group_chat:
		return reply(errors=["Group_chat_id is not valid"])
	js_admins = ast.literal_eval(db_group_chat.admins)
	js_members = ast.literal_eval(db_group_chat.members)
	db_admins = js_admins["admins"]
	db_members = js_members["members"]
	user_is_admin = (from_id in db_admins)
	if from_id not in db_members:
		return reply(errors=["You are not in this group chat"], resp_code=403)
	if add_members or add_admins:
		if not user_is_admin:
			return reply(errors=["You are not admin in this group chat"], resp_code=403)
		else:
			if add_members:
				for member in add_members:
					db_member = User.query.filter_by(id=member).first()
					if db_member:
						if member not in db_members:
							db_members.append(member)
					else:
						return reply(errors=["Member id {member} does not exist"])
				db_group_chat.members = {'members': db_members}
			else:
				for admin in add_admins:
					db_admin = User.query.filter_by(id=member).first()
					if db_admin:
						if admin not in db_admins:
							db_members.append(member)
					else:
						return reply(errors=["Admin id {member} does not exist"])
				db_group_chat.admins = {'admins': db_admins}
			try:
				db.session.commit()
			except DatabaseError:
				db.session.rollback()
				return reply(errors=["Some error in db..."], resp_code=500)
			if add_members:
				return reply(success=True, data=["Members has been added successfully"], group_id=db_group_chat.id, resp_code=200)
			else:
				return reply(success=True, data=["Admins has been added successfully"], group_id=db_group_chat.id, resp_code=200)
	elif name:
		if user_is_admin:
			if len(name) > 128:
				return reply(errors=["Name of the group chat too long"])
			else:
				db_group_chat.name = name
				try:
					db.session.commit()
				except DatabaseError:
					db.session.rollback()
					return reply(errors=["Some error in db..."], resp_code=500)
				return reply(success=True, data=["Name has been changed successfully"], group_id=db_group_chat.id, resp_code=200)
		else:
			return reply(errors=["You are not admin in this group chat"], resp_code=403)
	elif avatar_id:
		if user_is_admin:
			db_file = File.query.filter_by(id=avatar_id).first()
			if db_file:
				db_group_chat.avatar = avatar_id
				try:
					db.session.commit()
				except DatabaseError:
					db.session.rollback()
					return reply(errors=["Some error in db..."], resp_code=500)
				return reply(success=True, data=["Avatar has been changed successfully"], group_id=db_group_chat.id, resp_code=200)
			else:
				return reply(errors=["Avatar_id is not valid"])
		else:
			return reply(errors=["You are not admin in this group chat"], resp_code=403)


@app.route('/api/v1/group_chats/get', methods=['POST'])
def get_group_chat():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	group_chat_id = data.get("group_chat_id")
	db_group_chat = Group_chat.query.filter_by(id=group_chat_id).first()
	if not db_group_chat:
		return reply(errors=["Group_chat_id is invalid"])
	json_resp = []
	js_admins = ast.literal_eval(db_group_chat.admins)
	js_members = ast.literal_eval(db_group_chat.members)
	admins = js_admins["admins"]
	members = js_members["members"]
	js_resp = {
	    "id": db_group_chat.id,
	    "name": db_group_chat.name,
	    "avatar": get_url_for_file(get_file_by_id(db_group_chat.avatar)),
	    "members": members,
	    "admins": admins,
	}
	json_resp.append(js_resp)
	return reply(success=True, data=json_resp, resp_code=200)


@app.route('/api/v1/group_chats/get_all', methods=['POST'])
def get_all_group_chats():
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	db_group_chats = Group_chat.query.all()
	if not db_group_chats:
		return []
	json_resp = []
	for db_group_chat in db_group_chats:
		js_members = ast.literal_eval(db_group_chat.members)
		members = js_members["members"]
		js_admins = ast.literal_eval(db_group_chat.admins)
		admins = js_admins["admins"]
		if current_user.id in members:
			js_resp = {
			    "id": db_group_chat.id,
			    "name": db_group_chat.name,
			    "avatar": get_url_for_file(get_file_by_id(db_group_chat.avatar)),
			    "members": members,
			    "admins": admins,
			}
			json_resp.append(js_resp)
	return reply(success=True, data=json_resp, resp_code=200)
