from flask import request
from flask_login import current_user
from ymsapp import app, csrf
from ymsapp.models import User, Group
from ymsapp.utils.response import reply
from ymsapp.utils.file import get_url_for_file, get_file_by_id
from ymsapp.utils.user import is_admin
from ymsapp.utils.tools import get_item, db_push
import ast
# from sqlalchemy.exc import DatabaseError
"""@app.route('/api/v1/users/get_', methods=['POST'])
@csrf.exempt
def edit_user_info():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=['You are not logged in'], resp_code=401)
	id = current_user.id
	login = data.get('login')
	avatar = data.get('avatar')
	firstname = data.get('firstname')
	lastname = data.get('lastname')
	groups = data.get('groups')"""


@app.route('/api/v1/users/add', methods=['POST'])
def add_user():
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	if not is_admin():
		pass
	# some code for add user


@app.route('/api/v1/users/edit', methods=['POST'])
def edit_user():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)

	result = {"ok": True, "errors": []}
	user_id = get_item(data, "user_id")
	avatar = get_item(data, "avatar", whitelist=["emptiness"])
	login = get_item(data, "login", length=128, whitelist=["emptiness"])
	password = get_item(data, "password", length=255, whitelist=["emptiness"])
	firstname = get_item(data, "firstname", length=80, whitelist=["emptiness"])
	lastname = get_item(data, "lastname", length=80, whitelist=["emptiness"])
	patronymic = get_item(data, "patronymic", length=80, whitelist=["emptiness"])
	email = get_item(data, "email", length=256, whitelist=["emptiness"])
	phone_number = get_item(data, "email", whitelist=["emptiness"])
	additional_info = get_item(data, "additional_info", length=2048, whitelist=["emptiness"])
	groups = get_item(data, "groups", length=2048, whitelist=["emptiness"])
	if not result["ok"]:
		return reply(errors=result["errors"])

	if current_user.id != user_id:
		return reply(errors=["Permission denied"])

	db_user = User.query.filter_by(id=user_id)
	if not db_user:
		return reply(errors=["User_id is not valid"])

	if groups:
		for group_id in groups:
			if not group_id:
				return reply(errors=["Group_id is not valid"])
			db_group = Group.query.filter_by(group_id)
			if not db_group or not db_group.id:
				return reply(errors=["Group_id is not valid"])

	if not (avatar or login or password or firstname or lastname or patronymic or email or phone_number or additional_info
	        or groups):
		return reply(errors=["Nothing to change"])

	if avatar:
		db_user.avatar = avatar
	if login:
		db_user.login = login
	if password:
		db_user.password = password
	if firstname:
		db_user.firstname = firstname
	if lastname:
		db_user.lastname = lastname
	if patronymic:
		db_user.patronymic = patronymic
	if email:
		db_user.email = email
	if phone_number:
		db_user.phone_number = phone_number
	if additional_info:
		db_user.additional_info = additional_info
	if groups:
		db_user.groups = str({"groups": groups})

	commit = db_push(id_label="user_id", success_msg="User successfully edited")
	return commit


@app.route('/api/v1/users/get_all', methods=['POST'])
@csrf.exempt
def get_all_users():
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	db_users = User.query.all()
	json_resp = []
	if db_users:
		for db_user in db_users:
			js_resp = {}
			js_resp["id"] = db_user.id
			js_resp["avatar"] = get_url_for_file(get_file_by_id(file_id=db_user.avatar))
			js_resp["login"] = db_user.login
			js_resp["firstname"] = db_user.firstname
			js_resp["lastname"] = db_user.lastname
			js_resp["patronymic"] = db_user.patronymic
			js_resp["groups"] = ast.literal_eval(db_user.groups)["groups"]
			json_resp.append(js_resp)
	return reply(success=True, data=json_resp, resp_code=200)


@app.route('/api/v1/users/get', methods=['POST'])
def get_user_info():
	data = request.get_json()
	if not current_user.is_authenticated:
		return reply(errors=["You are not logged in"], resp_code=401)
	id = current_user.id
	other_id = data.get("user_id")
	if other_id:
		id = other_id
	db_user = User.query.filter_by(id=id).first()
	if not db_user:
		return reply(errors=["User_id is not valid"])
	json_resp = []
	if db_user:
		js_resp = {}
		js_resp["id"] = db_user.id
		js_resp["avatar"] = get_url_for_file(get_file_by_id(file_id=db_user.avatar))
		js_resp["login"] = db_user.login
		js_resp["firstname"] = db_user.firstname
		js_resp["lastname"] = db_user.lastname
		js_resp["patronymic"] = db_user.patronymic
		js_resp["email"] = db_user.email
		js_resp["phone_number"] = db_user.phone_number
		js_resp["additional_info"] = db_user.additional_info
		js_resp["groups"] = ast.literal_eval(db_user.groups)["groups"]
		json_resp.append(js_resp)
	else:
		return reply(errors=["User_id is not exists"])
	return reply(success=True, data=json_resp, resp_code=200)
