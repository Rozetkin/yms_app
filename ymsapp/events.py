from flask_socketio import emit, join_room, leave_room, disconnect
from flask_login import current_user
from ymsapp.utils.group_chat import get_group_chat_members
from ymsapp.decorators import authed_io_only
from ymsapp import socketio


@socketio.on('joined', namespace='/chat')
@authed_io_only
def joined(message):
	"""Sent by clients when they enter a room.
    A status message is broadcast to all people in the room."""
	from_id = current_user.id
	room = str(from_id)
	join_room(room)
	emit('status', {'msg': str(current_user.login) + ' has entered the room.'}, room=room)


@socketio.on('add', namespace='/chat')
@authed_io_only
def add(message):
	"""Sent by a client when the user entered a new message.
    The message is sent to all people in the room."""
	from_id = current_user.id
	from_id = int(from_id)
	to_id, to_chat_group_id = None, None
	if message['to_id']:
		to_id = message['to_id']
	if message['to_chat_group_id']:
		to_chat_group_id = message['to_chat_group_id']
	if not to_id and not to_chat_group_id:
		return disconnect()
	if to_chat_group_id:
		rooms = get_group_chat_members(to_chat_group_id)
		if rooms is None:
			return disconnect()
	else:
		if from_id != to_id:
			rooms = [from_id, to_id]
		else:
			rooms = [from_id]
	if not message['msg_id']:
		return disconnect()
	if not to_id:
		to_id = to_chat_group_id
	for room in rooms:
		emit('message', {'from': str(from_id), 'to': str(to_id), 'msg_id': message['msg_id']}, room=str(room))


@socketio.on('left', namespace='/chat')
@authed_io_only
def left(message):
	"""Sent by clients when they leave a room.
    A status message is broadcast to all people in the room."""
	from_id = current_user.id
	from_id = int(from_id)
	room = from_id
	leave_room(room)
	emit('status', {'msg': str(current_user.login) + ' has left the room.'}, room=room)
