/**
 * 
 * @param {Object} args An arguments for XMLHttpRequest
 * @param {String} action An action
 * @param {Function} f A function for handler 'onreadystatechange'
 */
function makeXHR(args, action, contentTypeValue, f) {
    let xhr = new XMLHttpRequest();

    xhr.open('POST', '/api/v1/' + action, true);
    xhr.setRequestHeader('Content-type', contentTypeValue + '; charset=utf-8');

    xhr.responseType = "json";

    // Отсылаем объект в формате JSON и с Content-Type application/json
    // Сервер должен уметь такой Content-Type принимать и раскодировать
    xhr.setRequestHeader("X-CSRFToken", csrf_token);

    xhr.onreadystatechange = (e) => {
        f(xhr);
    }

    xhr.send(JSON.stringify(args));
}
