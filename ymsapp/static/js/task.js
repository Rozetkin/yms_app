class Task {
    constructor(taskXHR) {
        this.id = taskXHR.id;
        this.text = taskXHR.text;
        this.method = taskXHR.method;
        this.fileIds = taskXHR.file_ids;
    }
}