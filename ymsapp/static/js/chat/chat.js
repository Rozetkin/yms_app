class Chat {
    constructor(name, type, id, avatarSrc, avatarSrcs) {
        this.name = name;
        this.type = type;
        this.id = id;
        this.avatarSrc = avatarSrc;
        this.avatarSrcs = avatarSrcs;
        this.store = {};
    }

    /**
     * 
     * @param {ChatMessage} message 
     */
    add(message) {
        this.store[message.id] = message;
    }

    makeMessagesUI(parent) {
        let storeList = Object.values(this.store);
        for (let message of storeList) {
            parent.append(message.getUI(this.avatarSrcs));
        }
    }

    getUI(fn, number) {
        return (
            $('<li></li>')
                .click(() => fn(number))
                .attr({ id: '' + user.id })
                .addClass('hoverable')
                .prepend(
                    $('<div></div>')
                        .addClass('d-flex bd-highlight')
                        .prepend(
                            $('<div></div>')
                                .addClass('img_cont')
                                .prepend(
                                    $('<img>')
                                        .addClass('circle user_img')
                                        .attr({
                                            src: this.avatarSrc,
                                            width: 50,
                                            height: 50
                                        })
                                ),
                            $('<div></div>')
                                .addClass('user_info')
                                .prepend(
                                    $('<span></span>').text(this.name),
                                    $('<p></p>').text('* left * ago')
                                )
                        )
                )
        );
    }
}
