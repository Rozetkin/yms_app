class SendMessage {
    constructor(fileID, text) {
        this.fileID = fileID;
        this.text = text;
    }
}


class ChatMessage {
    /**
     *  
     * @param {Object} messageXHR The message received by XHR
     * @param {String} messageXHR.fileUrl The url of chosen file, applied to message
     * @param {String} messageXHR.text The text, applied to message
     * @param {Number} messageXHR.from_id The id of sender user
     * @param {Number} messageXHR.to_id The id of receiver user
     * @param {Number} messageXHR.to_chat_group_id The id of receiver group
     * @param {Number} messageXHR.id The id of message
     * @param {Boolean} messageXHR.is_changed The mark of change message
     * @param {Boolean} messageXHR.is_deleted The mark of delete message
     * @param {Number} clientID The id of client
     */
    constructor(messageXHR, clientID) {
        this.content = {
            text: messageXHR.text,
            fileUrl: messageXHR.fileUrl,
        };
        this.align = clientID == messageXHR.from_id;
        this.senderID = messageXHR.from_id;
        this.recieverUserID = messageXHR.to_id;
        this.recieverGroupID = messageXHR.to_chat_group_id;
        this.changed = messageXHR.is_changed;
        this.deleted = messageXHR.is_deleted;
        this.time = messageXHR.time;
        // this.id = messageXHR.id;
    }

    getUI(avatarSrcs) {
        let id = this.recieverUserID;
        if (id == undefined) {
            id = this.recieverGroupID;
        }
        let div1 = $('<div></div>')
                        .addClass('img_cont_msg')
                        .append(
                            $('<img>').attr({
                                src: avatarSrcs[id],
                                width: 50,
                                height: 50,
                            }).addClass('rounded-circle user_img_msg')
                        );
        let div2 = $('<div></div>')
                        .addClass('msg_cotainer' + (this.align ? '_send' : ''))
                        .append(
                            $('<pre></pre>')
                            .text(this.content.text)
                        )
                        .append(
                            $('<span></span>')
                            .text(this.time)
                            .addClass('msg_time' + (this.align ? '_send' : ''))
                        );
        return $('<div></div>')
                    .addClass('d-flex mb-4 justify-content-' + (this.align ? 'end' : 'start'))
                    .prepend(this.align ? div2 : div1)
                    .append(this.align ? div1 : div2);
    }
}
