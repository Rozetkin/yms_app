$(() => {
    let intl = new Intl.DateTimeFormat('ru-RU', {
        year: '2-digit', month: 'numeric', day: 'numeric',
        hour: 'numeric', minute: 'numeric',
        hour12: false
    });
    let localUser = new LocalUser();
    let users = {};
    let subjects = {};
    let tasks = {};
    $.postJSON('api/v1/subjects/get_all', {}, data => {
        data.data.forEach(subject => {
            subjects[subject.id] = subject.name;
        });
    });
    $.postJSON('api/v1/tasks/get_all', {}, data => {
        data.data.forEach(task => {
            tasks[task.id] = new Task(task);
        });
        console.log(tasks);
    });
    $.postJSON('api/v1/users/get_all', {}, data => {
        data.data.forEach(user => {
            users[user.id] = new PublicUser(user);
        });
        console.log(users);
    }, false);
    events = [];
    $.postJSON('api/v1/events/get_all', (localUser.groups.has(14) ? { is_admin_get: true } : {}), data => {
        $('#thead').empty();
        $('#tbody').empty();
        $('#thead').append(
            '<tr>\
                <th scope="col" class="head-cell has-text-grey-darker">Дата</th>\
                <th scope="col" class="head-cell has-text-grey-darker">Предмет</th>\
                <th scope="col" class="head-cell has-text-grey-darker">Тип мероприятия</th>\
                <th scope="col" class="head-cell has-text-grey-darker">Описание</th>\
                <th scope="col" class="head-cell has-text-grey-darker">Ссылка</th>\
                <th scope="col" class="head-cell has-text-grey-darker">Описание дз</th>\
                <th scope="col" class="head-cell has-text-grey-darker">Способ сдачи дз</th>\
                <th scope="col" class="head-cell has-text-grey-darker">Преподаватель</th>\
            </tr>'
        );
        let number = 1;
        data.data.forEach(event => {
            let authorName = null;
            if (event.author)
                authorName = users[event.author].getFullName();
            let additionalAuthorName = null;
            if (event.additional_author)
                additionalAuthorName = users[event.additional_author].getFullName();
            event.author_name = authorName;
            event.additional_author_name = additionalAuthorName;
            event.subject = subjects[event.subject];
            event.task = tasks[event.task];
            event = new Event(event);
            $('#tbody').append(event.getLineUI(intl));
            events.push(event);
        });
        fillUI(intl, events, 'tbody', localUser, false);
        console.info(events);
    });
    $('#isRaw').change(() => fillUI(intl, events, 'tbody', localUser, false));
});
