class Event {
	constructor(event) {
		this.id = event.id;
		this.gsID = event.google_sheet_line;
		this.types = new Set(event.types);
		this.startTime = new Date(event.time);
		this.startTime.setMinutes(this.startTime.getMinutes() + this.startTime.getTimezoneOffset());
		// this.offset = this.startTime.getTimezoneOffset();
		this.subject = event.subject;
		this.description = event.description;
		this.links = event.links;
		this.authorID = event.author;
		this.authorName = event.author_name;
		this.additionalAuthorID = event.additional_author;
		this.additionalAuthorName = event.additional_author_name;
		this.task = event.task || {};
		this.forGroups = new Set(event.for_groups);
		this.rawData = event.unparsed_data;
		this.tags = new Set(event.tags);
		this.delayTime = new Date(0);
		if (this.types.has('вебинар')) {
			this.delayTime = new Date(2 * 60 * 60);
		}
	}

	getLineUI(intl) {
		let isRaw = $('#isRaw').is(':checked');
		let startTime = isRaw ? (this.rawData.time || "") : intl.format(this.startTime);
		let subject = isRaw ? (this.rawData.subject || "") : this.subject;
		let types = isRaw ? (this.rawData.types || "") : Array.from(this.types).join(' ');
		let description = isRaw ? (this.rawData.description || "") : this.description;
		let links = isRaw ? (this.rawData.links || "") : this.links;
		let authorName = isRaw ? (this.rawData.authors || "") : $('<a></a>').text(this.authorName).attr({
			href: './../profile/' + this.authorID
		});
		let additionalAuthorName = isRaw ? "" : $('<a></a>').text(this.additionalAuthorName).attr({
			href: './../profile/' + this.additionalAuthorID
		});
		let text = this.task.text;
		let method = this.task.method;
		return $('<tr></tr>').append(
			$('<td class="ceil no-wrap"></td>').text(startTime),
			$('<td class="ceil word-wrap is-capitalized"></td>').text(subject),
			$('<td class="ceil word-wrap is-capitalized"></td>').text(types),
			$('<td class="ceil word-wrap"></td>').text(description),
			$('<td class="ceil word-wrap"></td>').html(links.autoLink({
				target: '_blank'
			})),
			$('<td class="ceil word-wrap"></td>').text(text),
			$('<td class="ceil word-wrap"></td>').text(method),
			$('<td class="ceil word-wrap"></td>').append(authorName, (this.additionalAuthorID ? $('<div></div>').append($('<span> </span>'), additionalAuthorName) : ''))
		);
	}

	toggleModal(flag) {
		$('html').toggleClass('is-clipped', flag);
		$('#' + this.id).toggleClass('is-active', flag);
	}

	send(intl, user, events) {
		$.postJSON('/api/v1/events/del_tag', {
			event_id: this.id,
			tag: 'failed'
		}, data => {
			console.log(data);
		});
		$.postJSON('/api/v1/events/del_tag', {
			event_id: this.id,
			tag: 'expired'
		}, data => {
			console.log(data);
			// fillUI(intl, events, 'cards', user, true);
		});
		$.postJSON('/api/v1/events/add_tag', {
			event_id: this.id,
			tag: 'sent'
		}, data => {
			console.log(data);
			// fillUI(intl, events, 'cards', user, true);
		});
		this.tags.add('sent');
		this.tags.delete('failed');
		this.tags.delete('expired');
		fillUI(intl, events, 'cards', user, true);
	}

	getBoxUI(intl, user, events) {
		let links = '<pre class="word-wrap">' + this.links.autoLink() + '</pre>';
		let href = $(links).find('a').attr('href');
		let types = [];

		let hasTask = this.task.method || this.task.text || this.types.has('дедлайн');
		let isTeacher = user.groups.has(14);
		let isSent = this.tags.has('sent');

		this.types.forEach(type => {
			let tag_color;
			switch (type) {
				case 'дедлайн':
					tag_color = 'is-danger';
					break;
				case 'вебинар':
					tag_color = 'is-info';
					break;
				default:
					tag_color = 'is-grey';
					break;
			}
			let tag = $('<div class="tag has-text-right"></div>').addClass(tag_color).text(type);
			types.push(tag.prop('outerHTML'));
		});
		// let tags = types.join('<br>');
		let authorNameLinkDOM = $('<a></a>').text(this.authorName).attr({
			href: './../profile/' + this.authorID
		});
		let authorNameDOM = $('<div class="media"></div>').append($('<div class="media-content"></div>').append($('<h4 class="title is-4"></h4>').html(authorNameLinkDOM)));
		let eventDescriptionDOM = $('<div class="content"></div>').text(this.description).prepend('<label class="label">Описание занятия</label>');
		let gsIDDOM = $('<div class="content"></div>').text(this.gsID + ' + ' + this.id).prepend('<label class="label">Гугл шит айди</label>');
		let taskDescriptionDOM = $('<div class="content"></div>').text(this.task.text).prepend('<label class="label">Описание задания</label>');
		let taskMethodDOM = $('<div class="content"></div>').text(this.task.method).prepend('<label class="label">Метод сдачи</label>');
		let linkDOM = $('<div class="content"></div>').html(links).prepend('<label class="label">Ссылка</label>');
		let startTimeDOM = $('<div class="content"></div>').append('<label class="label">Дата</label>', $('<startTime></startTime>').text(intl.format(this.startTime)));
		let sendButtonDOM = $('<button class="button is-success">Отправил(а)</button>').click(() => this.send(intl, user, events));
		let openButtonDOM = $('<a class="button is-link" target="_blank"></a>').attr({
			href: href
		}).append($('<span class="icon"></span>').append('<i class="mdi mdi-24px mdi-open-in-new"><i>'), '<span>Открыть ссылку</span>');
		let cancelButtonDOM = $('<button class="button is-pulled-right">Выйти</button>').click(() => this.toggleModal(this.id, false));
		// let doneButtonDOM		= $('<button class="button is-success">Done</button>').click(() => this.done());
		// let failButtonDOM		= $('<button class="button is-danger">Failed</button>').click(() => this.failed());

		let eventDescriptionRes = this.description ? eventDescriptionDOM : undefined;
		let gsIDRes = user.groups.has(13) ? gsIDDOM : undefined;
		let taskDescriptionRes = this.task.text ? taskDescriptionDOM : undefined;
		let taskMethodRes = this.task.method ? taskMethodDOM : undefined;
		let linkRes = this.links ? linkDOM : undefined;
		let startTimeRes = this.startTime ? startTimeDOM : undefined;
		let openButtonRes = href ? openButtonDOM : undefined;
		let sendButtonRes = !isTeacher && hasTask && !isSent ? sendButtonDOM : undefined;

		let header = $('<header class="modal-card-head"></header>').append(
			$('<p class="modal-card-title is-marginless"></p>').text(this.subject),
			$('<div class="is-marginless is-right"></div>').append(types.join('&nbsp;'))
			// $('<button class="delete" aria-label="close" onclick="toggleModal(' + this.id + ', false)"></button>')
		);

		let body = $('<section class="modal-card-body"></section>').append(
			authorNameDOM,
			eventDescriptionRes,
			gsIDRes,
			taskDescriptionRes,
			taskMethodRes,
			linkRes,
			startTimeRes
		);

		let footer = $('<footer class="modal-card-foot"></footer>').append(
			sendButtonRes,
			openButtonRes,
			// doneButtonRes,
			// failButtonRes,
			cancelButtonDOM,
		);

		let modal = $('<div class="modal"></div>').attr({
			id: this.id
		}).append(
			$('<div class="modal-background"></div>').click(() => this.toggleModal(false)),
			$('<div class="modal-card fixed-max-width"></div>').append(
				header,
				body,
				footer
			)
		);

		let box = $('<div class="myhoverable box"></div>').attr({
			id: 'event' + this.id
		}).click(() => this.toggleModal(true)).append(
			$('<div class="media"></div>').append(
				$('<div class="media-left"></div>').append(
					$('<a target="_blank"></a>').append(
						$('<span class="is-large icon has-text-link"></span>').addClass(href ? '' : 'fa-disabled').append(
							$('<i class="mdi mdi-48px mdi-open-in-new"><i>')
						)
					).attr({
						href: href
					})
				),
				$('<div class="media-content"><div>').append(
					$('<div class="media-item"></div>').append(
						$('<div class="container"></div>').append(
							$('<p class="title is-5"></p>').text(this.subject),
							$('<p class="subtitle is-5"></p>').text(intl.format(this.startTime))
						)
					)
				),
				$('<div class="media-right"><div>').append(
					$('<div class="media-item"></div>').append(
						$('<div class="container"></div>').append(types.join('<br>'))
					)
				),
			)
		);

		return $('<div></div>').addClass(
			'column is-one-third-widescreen is-half-desktop is-half-tablet is-full-mobile'
		).append(modal, box);
	}
}

function fillUI(intl, events, id, localUser, flag) {
	console.time('start filtering');
	events.sort((a, b) => {
		if (mode === 'current' || !mode) {
			return a.startTime - b.startTime;
		} else {
			return b.startTime - a.startTime;
		}
	});
	$('#' + id).empty();
	let f = null;
	if (flag) {
		f = (event) => event.getBoxUI(intl, localUser, events);
	} else {
		f = (event) => event.getLineUI(intl);
	}
	events = events.filter(event => {
		switch (mode) {
			case 'expired': { // For teachers, but students
				let expired = event.tags.has('expired');
				let res = expired;
				if (res)
					$('#' + id).append(f(event, intl, localUser));
				return res;
			}
			case 'failed': { // For students
				let fit = event.tags.has('failed') || (event.tags.has('expired') && !event.tags.has('sent'));
				let res = fit;
				if (res)
					$('#' + id).append(f(event, intl, localUser));
				return res;
			}
			case 'sent': { // For students
				let sent = event.tags.has('sent');
				let res = sent;
				if (res)
					$('#' + id).append(f(event, intl, localUser));
				return res;
			}
			case 'current': { // For both
				let current = !event.tags.has('expired');
				let fit = event.tags.size == 1;
				let res = current && fit;
				if (res)
					$('#' + id).append(f(event, intl, localUser));
				return res;
			}
			default: {
				$('#' + id).append(f(event, intl, localUser));
				return true;
			}
		}
	});
	console.timeEnd('start filtering');
}