let firstname = $('#firstname');
let firstnameInp = firstname.children(':input');
// let firstnameHelp = firstname.parent().parent();

let lastname = $('#lastname');
let lastnameInp = lastname.children(':input');

let patronymic = $('#patronymic');
let patronymicInp = patronymic.children(':input');

let nameHelp = $('#nameHelp');

let email = $('#email');
let emailInp = email.children(':input');
let emailHelp = $('#emailHelp');

let tel = $('#tel');
let telInp = tel.children(':input');
let telHelp = $('#telHelp');

let login = $('#login');
let loginInp = login.children(':input');
let loginHelp = $('#loginHelp');

let password = $('#password');
let passwordInp = password.children(':input');
let passwordHelp = $('#passwordHelp');

let password2 = $('#password2');
let password2Inp = password2.children(':input');
let password2Help = $('#password2Help');

let teacherInp = $('#isTeacher');
let teacherHelp = $('#isTeacherHelp');

let captchaHelp = $('#captchaHelp');

nameHelp.text(nameError);
captchaHelp.text(captchaError);
loginHelp.text(loginError);
emailHelp.text(emailError);
telHelp.text(telError);
passwordHelp.text(passwordError);
password2Help.text(passwordError);

function firstnameCheck() {
	if (firstnameInp.val().length > 0) {
		firstnameInp.addClass('is-success');
		firstnameInp.removeClass('is-danger');
		firstname.find('span.is-right').remove();
		firstname.append('<span class="icon is-small is-right has-text-success">\
							<i class="fas mdi mdi-24px mdi-check"></i>\
						</span>');
	} else {
		firstnameInp.addClass('is-danger');
		firstnameInp.removeClass('is-success');;
		firstname.find('span.is-right').remove()
		firstname.append('<span class="icon is-small is-right has-text-danger">\
							<i class="fas mdi mdi-24px mdi-alert"></i>\
						</span>');
	}
}

function lastnameCheck() {
	if (lastnameInp.val().length > 0) {
		lastnameInp.addClass('is-success');
		lastnameInp.removeClass('is-danger');
		lastname.find('span.is-right').remove();
		lastname.append('<span class="icon is-small is-right has-text-success">\
							<i class="fas mdi mdi-24px mdi-check"></i>\
						</span>');
	} else {
		lastnameInp.addClass('is-danger');
		lastnameInp.removeClass('is-success');
		lastname.find('span.is-right').remove();
		lastname.append('<span class="icon is-small is-right has-text-danger">\
							<i class="fas mdi mdi-24px mdi-alert"></i>\
						</span>');
	}
}

function patronymicCheck() {
	if (patronymicInp.val().length > 0) {
		patronymicInp.addClass('is-success');
		patronymicInp.removeClass('is-danger');
		patronymic.find('span.is-right').remove();
		patronymic.append('<span class="icon is-small is-right has-text-success">\
							<i class="fas mdi mdi-24px mdi-check"></i>\
						</span>');
		teacherHelp.text('');
	} else if (teacherInp.is(':checked')) {
		teacherHelp.text('Укажите отчество');
		patronymicInp.addClass('is-danger');
		patronymicInp.removeClass('is-success');
		patronymic.find('span.is-right').remove();
		patronymic.append('<span class="icon is-small is-right has-text-danger">\
							<i class="fas mdi mdi-24px mdi-alert"></i>\
						</span>');
	} else {
		patronymicInp.removeClass('is-danger');
		patronymicInp.removeClass('is-success');
		patronymic.find('span.is-right').remove();
	}
}

function emailCheck() {
	if (emailInp.val().length > 0 && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailInp.val())) {
		emailHelp.text('');
		emailInp.addClass('is-success');
		emailInp.removeClass('is-danger');
		email.find('span.is-right').remove();
		email.append('<span class="icon is-small is-right has-text-success">\
		<i class="fas mdi mdi-24px mdi-check"></i>\
		</span>');
	} else {
		emailHelp.text('Укажите действительную почту');
		emailInp.addClass('is-danger');
		emailInp.removeClass('is-success');
		email.find('span.is-right').remove();
		email.append('<span class="icon is-small is-right has-text-danger">\
						<i class="fas mdi mdi-24px mdi-alert"></i>\
					</span>');
	}
}

function telCheck() {
	if (telInp.val().length > 0) {
		// if (/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(telInp.val())) {
		if (/(\+7|7|8)+([0-9]){10}$/im.test(telInp.val())) {
			telHelp.text('');
			telInp.addClass('is-success');
			telInp.removeClass('is-danger');
			tel.find('span.is-right').remove();
			tel.append('<span class="icon is-small is-right has-text-success">\
							<i class="fas mdi mdi-24px mdi-check"></i>\
						</span>');
		} else {
			telHelp.text('Укажите действительный номер');
			telInp.addClass('is-danger');
			telInp.removeClass('is-success');;
			tel.find('span.is-right').remove()
			tel.append('<span class="icon is-small is-right has-text-danger">\
							<i class="fas mdi mdi-24px mdi-alert"></i>\
						</span>');
		}
	}
}

function loginCheck() {
	if (loginInp.val().length > 5) {
		loginHelp.text('');
		loginInp.addClass('is-success');
		loginInp.removeClass('is-danger');
		login.find('span.is-right').remove();
		login.append('<span class="icon is-small is-right has-text-success">\
							<i class="fas mdi mdi-24px mdi-check"></i>\
						</span>');
	} else {
		loginHelp.text('Логин слишком короткий');
		loginInp.addClass('is-danger');
		loginInp.removeClass('is-success');
		login.find('span.is-right').remove();
		login.append('<span class="icon is-small is-right has-text-danger">\
							<i class="fas mdi mdi-24px mdi-alert"></i>\
						</span>');
	}
}

function passwordCheck() {
	if (passwordInp.val().length > 6) {
		passwordHelp.text('');
		passwordInp.addClass('is-success');
		passwordInp.removeClass('is-danger');
		password.find('span.is-right').remove();
		password.append('<span class="icon is-small is-right has-text-success">\
							<i class="fas mdi mdi-24px mdi-check"></i>\
						</span>');
	} else {
		passwordHelp.text('Пароль слишком короткий');
		passwordInp.addClass('is-danger');
		passwordInp.removeClass('is-success');
		password.find('span.is-right').remove();
		password.append('<span class="icon is-small is-right has-text-danger">\
							<i class="fas mdi mdi-24px mdi-alert"></i>\
						</span>');
	}
}

function password2Check() {
	if (password2Inp.val().length > 6 && password2Inp.val() == passwordInp.val()) {
		password2Help.text('');
		password2Inp.addClass('is-success');
		password2Inp.removeClass('is-danger');
		password2.find('span.is-right').remove();
		password2.append('<span class="icon is-small is-right has-text-success">\
		<i class="fas mdi mdi-24px mdi-check"></i>\
		</span>');
	} else {
		password2Help.text('Пароли не совпадают');
		password2Inp.addClass('is-danger');
		password2Inp.removeClass('is-success');
		password2.find('span.is-right').remove();
		password2.append('<span class="icon is-small is-right has-text-danger">\
							<i class="fas mdi mdi-24px mdi-alert"></i>\
						</span>');
	}
}

teacherInp.change(() => {
	if (patronymicInp.val() <= 0 && teacherInp.is(':checked')) {
		teacherHelp.text('Укажите отчество');
		patronymicInp.addClass('is-danger');
		patronymic.append('<span class="icon is-small is-right has-text-danger">\
								<i class="fas mdi mdi-24px mdi-alert"></i>\
							</span>');
		patronymicInp.removeClass('is-success');
	} else if (patronymicInp.val() <= 0) {
		patronymicInp.removeClass('is-success');
		patronymicInp.removeClass('is-danger');
		patronymic.find('span.is-right').remove();
		teacherHelp.text('');
	} else {
		teacherHelp.text('');
	}
});

firstnameInp.blur(() => firstnameCheck());
firstnameInp.keyup(() => firstnameCheck());

lastnameInp.blur(() => lastnameCheck());
lastnameInp.keyup(() => lastnameCheck());

patronymicInp.blur(() => patronymicCheck());
patronymicInp.keyup(() => patronymicCheck());

emailInp.blur(() => emailCheck());
emailInp.keyup(() => emailCheck());

telInp.blur(() => telCheck());
telInp.keyup(() => telCheck());

loginInp.blur(() => loginCheck());
loginInp.keyup(() => loginCheck());

let flagp = false
passwordInp.blur(() => {
	passwordCheck();
	if (flagp2)
	password2Check();
	flagp = true;
});
passwordInp.keyup(() => {
	if (!flagp) return;
	passwordCheck();
	if (flagp2)
	password2Check()
});

let flagp2 = false
password2Inp.blur(() => {
	passwordCheck();
	if (flagp2)
	password2Check();
	flagp2 = true;
});
password2Inp.keyup(() => {
	if (!flagp2) return;
	passwordCheck();
	if (flagp2)
	password2Check()
});