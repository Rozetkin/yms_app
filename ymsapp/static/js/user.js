class PublicUser {
    /**
     * 
     * @param {Object} userXHR 
     * @param {Number} userXHR.id The id of user
     * @param {String} userXHR.avatar The source path to avatar image
     * @param {String} userXHR.login The login of user
     * @param {String} userXHR.firstname The firstname of user
     * @param {String} userXHR.lastname The lastname of user
     * @param {String} userXHR.email The email of user
     * @param {Number} userXHR.phone_number The phone number of user
     * @param {String} userXHR.additional_info The additional info of user
     * @param {String} userXHR.groups The user's group
     */
    constructor(userXHR) {
        userXHR = userXHR || {};
        this.id = userXHR.id;
        this.avatarSrc = userXHR.avatar;
        this.login = userXHR.login;
        this.firstname = userXHR.firstname;
        this.lastname = userXHR.lastname;
        this.patronymic = userXHR.patronymic;
        this.email = userXHR.email;
        this.phoneNumber = userXHR.phone_number;
        this.additionalInfo = userXHR.additional_info;
        this.groups = new Set(userXHR.groups);
    }

    getFullName() {
        return this.firstname + ' ' + this.lastname;
    }

    getTeacherName() {
        return this.firstname + ' ' + this.patronymic;
    }
}


class LocalUser extends PublicUser {
    constructor(socket) {
        let user = null;
        $.postJSON('/api/v1/users/get', {}, data => {
            user = data.data[0];
        }, false);
        super(user);
        this.socket = socket;
        this.chats = [{}, {}];
        this.currentChat = null;
    }

    /**
     * 
     * @param {SendMessage} message 
     * @param {Chat} chat
     */
    send(message, chat) {
        let args;
        args.from_id = this.id;
        if (chat.type) {
            args.to_id = chat.id;
        } else {
            args.to_chat_group_id = chat.id;
        }
        args.text = message.content.text;
        // args.file_id = message.content.fileID;

        $.postJSON('api/v1/messages/add', args, data => {
            this.socket.emit('add', {
                to_id: args.to_id,
                to_chat_group_id: args.to_chat_group_id,
                msg_id: data.message_id,
            });
        });
    }

    /**
     * @description Put it in socket.on('message') 
     * @param {Object} data 
     * @param {Number} data.msg_id 
     */
    update(data) {
        let args = {
            message_id: data.msg_id
        };
        
        $.postJSON('api/v1/messages/get', args, data => {
            for (let messageXHR of data.data) {
                this._handleMessage(messageXHR);
            }
        });
    }

    _handleMessage(messageXHR) {
        let message = new ChatMessage(messageXHR, this.id);
        if (messageXHR.to_id) {
            if (messageXHR.to_id == this.id) {
                this._putMessageInChat(message, 0, to_id);
            } else {
                this._putMessageInChat(message, 0, from_id);
            }
        } else {
            this._putMessageInChat(message, 1, to_group_chat_id);
        }
    }

    /**
     * 
     * @param {ChatMessage} message 
     * @param {Number} type 
     * @param {Number} chatID 
     */
    _putMessageInChat(message, type, chatID) {
        if (!this.chats[type][chatID]) {
            this.chats[type][chatID] = new Chat(type, chatID);
        }
        this.chats[type][chatID].add(message);
    }
}
