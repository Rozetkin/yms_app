$.postJSON = function(url, data, callback, async = true, contentType = 'application/json', dataType = 'json') {
    return jQuery.ajax({
        type: 'POST',
        url: url,
        contentType: contentType + '; charset=UTF-8',
        data: JSON.stringify(data),
        dataType: dataType,
        success: callback,
        async: async
    });
};