// (function() {
let socket, curUser, users = {},
    chats = [{}, {}],
    otherUser, otherUserDOM;
let textInput = document.getElementById('textInput');
let usersListDOM = document.getElementById('usersList');
let messagesListDom = document.getElementById('messagesList');

function parseXHRMessage(message) {
    return {
        content: {
            fileUrl: message.file_url,
            text: message.text,
        },
        align: curUser.id == message.from_id, // make align relative to sender
        changed: message.is_changed,
        deleted: message.is_deleted,
        time: message.time,
        id: message.id,
    };
}

function createChatGroup(name, members, admins, avatar = null) {
    let arguments = {
        name,
        members,
        admins,
        avatar,
    };

    makeXHR(arguments, 'group_chats/add', 'application/json', xhr => {
        if (xhr.readyState == 4) {
            let response = xhr.response;
            console.log(response);
        }
    })
}

function sendMessage(userID, groupID, {
    text = null,
    fileID = null,
}) {
    let arguments = {
        to_id: userID,
        to_chat_group_id: groupID,
        text: text,
        file_id: fileID,
    };

    makeXHR(arguments, 'messages/add', 'application/json', xhr => {
        if (xhr.status == 200 && xhr.readyState == 4) {
            let response = xhr.response;

            socket.emit('add', {
                to_id: userID,
                to_chat_group_id: groupID,
                msg_id: response.message_id,
            });
        }
    });
}

function editMessage(messageID, {
    text,
    fileID
}) {
    let arguments = {
        message_id: messageID,
        text: text,
        file_id: fileID,
    }

    makeXHR(arguments, "messages/edit", 'application/json', xhr => {

    })
}

/**
 * @description Call when message changed(added, deleted, edited, ...)
 */
function getMessage(data) {
    let arguments = {
        message_id: data.msg_id
    };

    makeXHR(arguments, 'messages/get', 'application/json', xhr => {
        if (xhr.readyState == 4) {
            let respone = xhr.response;

            if (!respone.success) {
                console.log(respone.errors[0] || 'error');
                return;
            }

            for (let message of respone.data) {
                if (message.to_id) {
                    if (otherUser && (otherUser.id == message.to_id || otherUser.id == message.from_id)) {
                        createMessageDOM(parseXHRMessage(message));
                    }

                    if (curUser.id == message.to_id) {
                        putMessageToUsersOrGroups(message, 0, message.from_id);
                    } else {
                        putMessageToUsersOrGroups(message, 0, message.to_id);
                    }
                } else if (message.to_group_chat_id) {} else {
                    console.error("error!")
                }
            }
        }
    });
}

function putMessageToUsersOrGroups(message, index, id) {
    chats[index]['' + id] = chats[index]['' + id] || {};
    chats[index]['' + id]['' + message.id] = parseXHRMessage(message);
}

/**
 * @description Loads all messages with user or group, call once at start
 * @param {Number} userID id of user
 * @param {Number} groupID id of group
 */
function getMessages(userID, groupID) {
    let arguments = {
        to_id: userID,
        to_group_chat_id: groupID,
    };

    makeXHR(arguments, 'messages/get_all', 'application/json', xhr => {
        if (xhr.readyState == 4) {
            let respone = xhr.response;

            if (!respone.success) {
                console.log(respone.errors[0] || 'error');
                return;
            }

            if (respone.data)
                for (let message of respone.data) {
                    if (message.to_id) {
                        if (curUser.id == message.to_id) {
                            putMessageToUsersOrGroups(message, 0, message.from_id);
                        } else {
                            putMessageToUsersOrGroups(message, 0, message.to_id);
                        }
                    } else if (message.to_group_chat_id) {
                        console.error("group!!!!");
                    } else {
                        console.error("error!");
                    }
                }
        }
    });
}

function enter() {
    if (!otherUser) {
        console.error('no user chosen');
        return;
    }
    let text = textInput.value;
    textInput.value = '';
    sendMessage(otherUser.id, null, {
        text
    });
}

// function leave_room() {
//     socket.emit('left', {
//         id: idInput.value
//     }, function() {
//         socket.disconnect();

//         // go back to the login page
//         window.location.href = "{{ url_for('//') }}";
//     });
// }

$(document).ready(() => {
    makeXHR({}, 'users/get', 'application/json', xhr => {
        if (xhr.readyState == 4) {
            curUser = xhr.response.data[0];
            console.log(curUser);
        }
    });

    makeXHR({}, 'users/get_all', 'application/json', xhr => {
        if (xhr.readyState == 4) {
            xhr.response.data.forEach(user => {
                users['' + user.id] = user;
                getMessages(user.id);
            });
            fillUsersList();
            console.log(users);
        }
    });

    socket = io.connect('http://' + document.domain + ':' + location.port + '/chat');

    socket.on('connect', () => {
        socket.emit('joined', {});
    });

    socket.on('message', function(data) {
        console.log(data);

        getMessage(data);
    });
});


function fillUsersList() {
    Object.values(users).forEach(user => {
        let img = $('<img>').attr({
            src: user.avatar || './../static/resources/img/yms.png',
            width: 50,
            height: 50,
        }).addClass('circle', 'user_img');
        let div1 = $('<div></div>').addClass('img_cont');
        div1.prepend(img);

        let span = $('<div></div>').text(user.firstname + ' ' + user.lastname);
        let p = $('<p></p>').text('* left * ago');
        let div2 = $('<div></div>').addClass('user_info');
        div2.prepend(span, p);

        let div = $('<div></div>').addClass('d-flex bd-highlight');
        div.prepend(div1, div2);

        let li = $('<li></li>');
        li.prepend(div);

        let number = usersListDOM.childElementCount;
        li.click(() => {
            chooseUser(number);
        });
        li.id = '' + user.id;
        li.addClass('hoverable');

        $('#usersList').append(li);
    });
}

function chooseUser(number) {
    if (otherUserDOM) {
        otherUserDOM.classList.remove(["active_chat"]);
    }
    otherUserDOM = usersListDOM.children.item(number);
    otherUserDOM.classList.add(["active_chat"]);
    otherUser = users[otherUserDOM.id];

    // fillMessagesList();
}

function fillMessagesList() {
    $('messagesList').empty();
    let messagesListObject = chats[0][otherUser.id];
    if (!messagesListObject) return;
    let messagesList = Object.values(messagesListObject);
    console.log(messagesList);
    messagesList.forEach(message => {
        createMessageDOM(message);
    })
}


function createMessageDOM(message) {
    let img = document.createElement('img');
    img.src = message.align ? curUser.avatar : otherUser.avatar;
    img.classList.add(["rounded-circle"], ["user_img_msg"]);
    let div1 = document.createElement('div');
    div1.classList.add(["img_cont_msg"]);
    div1.appendChild(img);
    let span = document.createElement('span');
    span.innerHTML = message.time;
    span.classList.add(["msg_time" + (message.align ? "_send" : "")]);
    let div2 = document.createElement('div');
    div2.innerHTML = message.content.text;
    div2.classList.add(["msg_cotainer" + (message.align ? "_send" : "")]);
    div2.appendChild(span);
    let div3 = document.createElement('div');
    div3.classList.add(["d-flex"], ["justify-content-" + (message.align ? "end" : "start")], ["mb-4"]);
    if (message.align) {
        div3.appendChild(div2);
        div3.appendChild(div1);
    } else {
        div3.appendChild(div1);
        div3.appendChild(div2);
    }
    messagesListDom.appendChild(div3);
}
// })();
