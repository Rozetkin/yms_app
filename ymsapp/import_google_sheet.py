#from flask_login import current_user
from ymsapp import db  # , app
from ymsapp.utils.user import (create_user, get_user_id_by_login, get_user_by_id, get_user_ids_by_lfp, is_teacher)
from ymsapp.models import Task, Subject, Event
import requests
from random import randrange
from transliterate import translit
from datetime import datetime
# import os
import ast
from ymsapp.utils.email import sendmail
from ymsapp.utils.tools import get_time, localtz
# from ymsapp.decorators import multi_threading
from ymsapp.utils.tools.name_parser import parse
from ymsapp.utils.group import create_db_group
from ymsapp.links import names_db, names_exceptions, subject_links_s, type_links_s
from ymsapp.decorators import multi_threading


def unify(string_item=None):
	if string_item:
		return string_item.replace('-', ' ').replace('.', ' ').replace(' ', '').lower()
	return None


def create_db_subject(name=None):
	if name:
		db_subject = Subject.query.filter_by(name=name).first()
		if db_subject:
			return db_subject
		subject = Subject(name=name)
		db.session.add(subject)
		db.session.commit()
		return subject
	else:
		return None


def create_db_event(google_sheet_line=None,
                    subject_id=None,
                    types=None,
                    description=None,
                    links=None,
                    author=None,
                    time=None,
                    additional_author=None,
                    task_id=None,
                    for_groups=None,
                    unparsed_data={
                        "subject": [],
                        "types": [],
                        "time": [],
                        "description": [],
                        "links": [],
                        "authors": [],
                        "for_groups": []
                    }):
	# if not types or not for_groups:
	# 	if not types and unparsed_data['types'] is not [] and not for_groups:
	# 		pass
	# 	else:
	# 		return None
	if google_sheet_line:
		test_event = Event.query.filter_by(google_sheet_line=google_sheet_line).first()
		if test_event:
			if test_event.subject != subject_id:
				test_event.subject = subject_id
			if test_event.types != str({"types": types}):
				test_event.types = str({"types": types})
			if test_event.description != description:
				test_event.description = description
			if test_event.links != links:
				test_event.links = links
			if test_event.author != author:
				test_event.author = author
			if test_event.time is not time:
				test_event.time = time
			if test_event.additional_author != additional_author:
				test_event.additional_author = additional_author
			if test_event.task != task_id:
				test_event.task = task_id
			if test_event.for_groups != str({"for_groups": for_groups}):
				test_event.for_groups = str({"for_groups": for_groups})
			test_event.unparsed_data = str(unparsed_data)
			db.session.commit()
			return test_event
	new_event = Event(google_sheet_line=google_sheet_line,
	                  subject=subject_id,
	                  types=str({"types": types}),
	                  description=description,
	                  links=links,
	                  author=author,
	                  time=time,
	                  additional_author=additional_author,
	                  task=task_id,
	                  for_groups=str({"for_groups": for_groups}),
	                  unparsed_data=str(unparsed_data))
	db.session.add(new_event)
	db.session.commit()
	return new_event


def create_db_task(google_sheet_line=None, text=None, method=None):
	if text or method:
		if google_sheet_line:
			test_event = Event.query.filter_by(google_sheet_line=google_sheet_line).first()
			if test_event:
				if test_event.task:
					task = Task.query.filter_by(id=test_event.task).first()
					if task.text != text:
						task.text = text
					if task.method != method:
						task.method = method
					db.session.commit()
					return task
		task = Task(text=text, method=method)
		db.session.add(task)
		db.session.commit()
		return task
	return None


def gen_teacher_login():
	while True:
		login = "teacher_" + str(randrange(0, 500, 1))
		db_user_id = get_user_id_by_login(login=login)
		if not db_user_id:
			return login


def make_user(teacher=None):
	if teacher is None or teacher == " ":
		return None
	patronymic = translit(teacher[0], reversed=True)
	firstname = translit(teacher[1], reversed=True)
	lastname = translit(teacher[2], reversed=True)
	email = patronymic + "." + firstname + "." + lastname + "@ymsapp.com"
	ids = get_user_ids_by_lfp(firstname=teacher[1], lastname=teacher[0], patronymic=teacher[2])
	teacher_id = None
	for id in ids:
		if is_teacher(user_id=id):
			teacher_id = id
			break
	if teacher_id is not None:
		db_user = get_user_by_id(id=teacher_id)
	else:
		db_user = create_user(avatar_id=None,
		                      login=gen_teacher_login(),
		                      password="super_secret_teachers_password",
		                      firstname=teacher[1],
		                      lastname=teacher[0],
		                      patronymic=teacher[2],
		                      email=email,
		                      groups=[create_db_group("Учитель").id])
	if db_user is not None:
		return db_user.id
	else:
		print(db_user, " ", teacher_id, " ", ids, " ", is_teacher(user_id=id))
		raise ValueError


def find_type_link(type=None):
	if type:
		for i in range(len(type_links_s)):
			find_link = [unify(type_links_s[i][0]), unify(type_links_s[i][1])]
			if unify(type) in find_link:
				return type_links_s[i][1]
	return None


def find_subject_link(subject=None):
	if subject:
		for i in range(len(subject_links_s)):
			find_link = [unify(subject_links_s[i][0]), unify(subject_links_s[i][1])]
			if unify(subject) in find_link:
				return subject_links_s[i][1]
		return None
	return None


def parse_datetime(string_datetime=None):
	if string_datetime is None:
		return get_time()
	else:
		ldt = string_datetime.replace(" ", ":").replace(".", ":").replace("-", ":").split(":")
		try:
			string_datetime = str(ldt[0]) + "." + str(ldt[1]) + "." + str(ldt[2]) + " " + str(ldt[3]) + ":" + str(ldt[4])
		except IndexError:
			return get_time()

	try:
		string_datetime = localtz.localize(datetime.strptime(string_datetime, "%d.%m.%Y %H:%M"))
	except ValueError:
		return get_time()
	return string_datetime

@multi_threading
def start():
	api_key_file = open("api.key", "r")
	api_key = api_key_file.read()
	api_key_file.close()

	params = (('key', api_key), )
	response = requests.get(
	    'https://sheets.googleapis.com/v4/spreadsheets/1qhhgjtlPai436u0_H8f5Ehf5PvzQqar9Aam-7HTSppY/values/base',
	    params=params)

	js_sheet = ast.literal_eval(response.text)
	lines = []
	sendmail("escherevatsky@gmail.com", None, "Import initialized by web  started at " + get_time().strftime("%d-%b-%Y (%H:%M:%S.%f)"), "Import google-sheet")
	print("**** Import started")
	for line in js_sheet['values']:
		try:
			sheet_id = line[11]
		except IndexError:
			sheet_id = ""
		if sheet_id == "":
			continue
		else:
			try:
				sheet_id = int(sheet_id)
			except ValueError:
				continue
		sheet_line = {
		    "class": line[0],
		    "datetime": line[1] + " " + line[2],
		    "subject": line[3],
		    "types": line[4],
		    "description": line[5],
		    "links": line[6],
		    "task_description": line[7],
		    "task_method": line[8],
		    "teachers": line[9],
		    "sheet_id": sheet_id
		}
		lines.append(sheet_line)

	for line_id, line in enumerate(lines):
		if line_id%50 == 0: 
			print("**** Import process: " + str(line_id) + "/" + str(len(lines)) + ". Google sheet line: " + str(line['sheet_id']))
		event_class = line['class']
		event_datetime = parse_datetime(line['datetime'])
		event_subject = line['subject']
		event_types = line['types']
		event_description = line['description']
		event_links = line['links']
		event_teachers = line['teachers']
		event_sheet_id = line['sheet_id']
		task_description = line['task_description']
		task_method = line['task_method']

		if task_description.replace(" ", "") or task_method.replace(" ", ""):
			event_types += " дедлайн"
		tmp_teachers = parse(input=event_teachers, db=names_db, exceptions=names_exceptions)
		event_teachers = []
		for tmp_teacher in tmp_teachers:
			if tmp_teacher['user'] is not None:
				event_teachers.append(tmp_teacher['user'])
			elif tmp_teacher.get("errors") is None:
				# some code for smart aliasing with class and subject
				pass
			else:
				# some code for send emails
				pass
		tmp_teachers.clear()
		for event_teacher in event_teachers:
			db_teacher_id = make_user(teacher=event_teacher)
			if db_teacher_id is not None:
				if db_teacher_id not in tmp_teachers:
					tmp_teachers.append(db_teacher_id)
		event_teachers = tmp_teachers
		if not event_subject:
			event_subject = "другое"
		event_subject = create_db_subject(name=event_subject).id
		tmp_event_types = unify(event_types).lower().split(",")
		event_types = []
		event_author, event_additional_author = None, None
		if len(event_teachers) > 0:
			event_author = event_teachers[0]
			if len(event_teachers) > 1:
				event_additional_author = event_teachers[1]

		if task_method or task_description:
			event_types.append("дедлайн")

		for tmp_event_type in tmp_event_types:
			event_types_link = find_type_link(type=tmp_event_type)
			if event_types_link not in event_types and event_types_link is not None and event_types_link != "ignore":
				event_types.append(event_types_link)

		if event_types is []:
			event_types.append("другое")

		db_group = create_db_group(group_name=event_class)
		if db_group is not None:
			event_class = db_group.id

		db_task_id = None
		if task_description or task_method:
			db_task_id = create_db_task(google_sheet_line=event_sheet_id, text=task_description, method=task_method).id

		event = create_db_event( # noqa 841
		    google_sheet_line=event_sheet_id,
		    subject_id=event_subject,
		    types=event_types,
		    description=event_description,
		    links=event_links,
		    author=event_author,
		    time=event_datetime,
		    additional_author=event_additional_author,
		    task_id=db_task_id,
		    for_groups=[event_class],
		    unparsed_data={
		        "subject": line['subject'],
		        "types": line['types'],
		        "time": line['datetime'],
		        "description": line['description'],
		        "links": line['links'],
		        "authors": line['teachers'],
		        "for_groups": line['class']
		    })
	sendmail("escherevatsky@gmail.com", None, "Import initialized by web succesfully ended at " + get_time().strftime("%d-%b-%Y (%H:%M:%S.%f)") + ".\n Totally events imported: " + str(len(lines)), "Import google-sheet")
	print("**** Import ended (" + str(len(lines))+")")
