from flask_login import UserMixin
from ymsapp import db, login_manager
from datetime import datetime
from ymsapp.utils.tools import get_time


class User(db.Model, UserMixin):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	avatar = db.Column(db.Integer, db.ForeignKey("files.id", ondelete="SET NULL"), default=None)
	login = db.Column(db.String(128), nullable=False, unique=True)
	password = db.Column(db.String(255), nullable=False)
	firstname = db.Column(db.String(80), nullable=False)
	lastname = db.Column(db.String(80), nullable=False)
	patronymic = db.Column(db.String(80))
	verified = db.Column(db.Boolean, default=False)
	email = db.Column(db.String(256), nullable=False, unique=True)
	phone_number = db.Column(db.String(12), default=None)
	additional_info = db.Column(db.String(2048))
	groups = db.Column(db.String(2048), default='{"groups": []}', nullable=False)

	def __init__(self, **kwargs):
		super(User, self).__init__(**kwargs)


class Notification(db.Model):
	__tablename__ = 'notifications'
	id = db.Column(db.Integer, primary_key=True, unique=True)
	text = db.Column(db.String(4096), nullable=False)
	attachments = db.Column(db.String(2048))
	from_id = db.Column(db.Integer, db.ForeignKey("users.id", ondelete="SET NULL"))
	is_changed = db.Column(db.Boolean, default=False)
	to_groups = db.Column(db.String(4096))


class Group(db.Model):
	__tablename__ = 'groups'
	id = db.Column(db.Integer, primary_key=True, unique=True)
	name = db.Column(db.String(128), nullable=False)


class Group_chat(db.Model):
	__tablename__ = 'group_chats'
	id = db.Column(db.Integer, primary_key=True, unique=True)
	name = db.Column(db.String(128), nullable=False)
	avatar = db.Column(db.Integer, db.ForeignKey("files.id", ondelete="SET NULL"), default=None)
	members = db.Column(db.String(4096), default='{"members": []}')
	admins = db.Column(db.String(4096), default='{"admins": []}')


class Subject(db.Model):
	__tablename__ = 'subjects'
	id = db.Column(db.Integer, primary_key=True, unique=True)
	name = db.Column(db.String(128), nullable=False, unique=True)


class User_event_tag(db.Model):
	__tablename__ = 'user_event_tags'
	id = db.Column(db.Integer, primary_key=True, unique=True)
	user_id = db.Column(db.Integer, db.ForeignKey("users.id", ondelete="SET NULL"), default=None)
	event_id = db.Column(db.Integer, db.ForeignKey("events.id", ondelete="SET NULL"), default=None)
	tags = db.Column(db.String(4096), default='{"tags": []}')


class Event(db.Model):
	__tablename__ = 'events'
	id = db.Column(db.Integer, primary_key=True, unique=True)
	google_sheet_line = db.Column(db.Integer, default=None)
	subject = db.Column(db.Integer, db.ForeignKey("subjects.id", ondelete="SET NULL"), default=None)
	types = db.Column(db.String(1024))
	time = db.Column(db.DateTime, default=get_time())
	description = db.Column(db.String(2048), default=None)
	links = db.Column(db.String(2048), default=None)
	author = db.Column(db.Integer, db.ForeignKey("users.id", ondelete="SET NULL"), default=None)
	additional_author = db.Column(db.Integer, db.ForeignKey("users.id", ondelete="SET NULL"), default=None)
	task = db.Column(db.Integer, db.ForeignKey("tasks.id", ondelete="SET NULL"), default=None)
	for_groups = db.Column(db.String(2048), default='{"for_groups": []}')
	unparsed_data = db.Column(
	    db.String(4096),
	    default='{"subject": [], "types": [], "time": [], "description": [], "links": [], "authors": [], "for_groups": []}'
	)


class Task(db.Model):
	__tablename__ = 'tasks'
	id = db.Column(db.Integer, primary_key=True, unique=True)
	text = db.Column(db.String(4096), default=None)
	method = db.Column(db.String(1024), default=None)
	file_ids = db.Column(db.String(2048), default='{"file_ids": "[]"}')
	# unparsed_data = db.Column(db.String(4096), default='{"text": [],"method": []}')


class Message(db.Model):
	__tablename__ = 'messages'
	id = db.Column(db.Integer, primary_key=True, unique=True)
	text = db.Column(db.String(2048))
	file_id = db.Column(db.Integer, db.ForeignKey("files.id", ondelete="SET NULL"))
	from_id = db.Column(db.Integer, db.ForeignKey("users.id", ondelete="SET NULL"))
	to_id = db.Column(db.Integer, db.ForeignKey("users.id", ondelete="SET NULL"))
	to_group_chat_id = db.Column(db.Integer, db.ForeignKey("group_chats.id", ondelete="SET NULL"))
	time = db.Column(db.DateTime, default=get_time())
	is_changed = db.Column(db.Boolean, default=False)
	is_deleted = db.Column(db.Boolean, default=False)


class File(db.Model):
	__tablename__ = 'files'
	id = db.Column(db.Integer, primary_key=True, unique=True)
	uid = db.Column(db.String(1024), nullable=False, unique=True)
	name = db.Column(db.String(2048), nullable=False)
	uploader_id = db.Column(db.Integer, db.ForeignKey("users.id", ondelete="SET NULL"))
	is_private = db.Column(db.Boolean)
	members = db.Column(db.String(4096), nullable=False, default='{"members": []}')


@login_manager.user_loader
def load_user(user_id):
	return User.query.get(user_id)
